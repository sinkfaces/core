package ru.tesker.core.common.server.properties;

import ru.tesker.core.api.server.properties.ServerProperty;

public class ServerPropertyImpl<T> implements ServerProperty<T> {

    private String id;
    private T value;

    ServerPropertyImpl(String id, T value) {
        this.id = id;
        this.value = value;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public T getValue() {
        return value;
    }

}
