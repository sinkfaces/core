package ru.tesker.core.common.server;

import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import ru.tesker.core.api.server.DefaultGlobalServerInfo;
import ru.tesker.core.api.server.ServerData;
import ru.tesker.core.api.server.ServerDataProvider;
import ru.tesker.core.common.entities.SharedPlayerImpl;

import java.net.InetAddress;
import java.util.*;

@Table(keyspace = "mixmine", name = "servers")
public class ServerDataImpl implements ServerData {


    private String id;
    @PartitionKey
    private InetAddress address;
    @PartitionKey(1)
    private int port;
    private int maxPlayers;
    private boolean enabled;
    private boolean online;
    private boolean canAddPlayers;
    private DefaultGlobalServerInfo globalServerInfo;
    private Collection<SharedPlayerImpl> players;
    private Map<String, String> properties = new HashMap<>();

    private ServerDataImpl() {
    }

    public ServerDataImpl(String id,
                          ServerDataProvider serverDataProvider,
                          DefaultGlobalServerInfo globalServerInfo,
                          Collection<SharedPlayerImpl> players,
                          Map<String, String> properties) {
        this.id = id;
        this.address = serverDataProvider.getIp().getAddress();
        this.port = serverDataProvider.getIp().getPort();
        this.maxPlayers = serverDataProvider.getMaxPlayers();
        this.enabled = serverDataProvider.isEnabled();
        online = true;
        this.canAddPlayers = serverDataProvider.canAddPlayers();
        this.globalServerInfo = globalServerInfo;
        this.players = players;
        this.properties = properties;
    }

    public String getId() {
        return id;
    }

    public InetAddress getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }

    public boolean isOnline() {
        return online;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isCanAddPlayers() {
        return canAddPlayers;
    }

    public DefaultGlobalServerInfo getGlobalServerInfo() {
        return globalServerInfo;
    }

    public Set<SharedPlayerImpl> getPlayers() {
        return Collections.unmodifiableSet(new HashSet<>(players));
    }

    @Override
    public Map<String, String> getProperties() {
        return ImmutableMap.copyOf(properties);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ServerDataImpl that = (ServerDataImpl) o;

        return new EqualsBuilder()
                .append(getPort(), that.getPort())
                .append(getAddress(), that.getAddress())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getAddress())
                .append(getPort())
                .toHashCode();
    }

    @Override
    public String toString() {
        return "ServerDataImpl{" +
                "id='" + id + '\'' +
                ", address=" + address +
                ", port=" + port +
                ", maxPlayers=" + maxPlayers +
                ", enabled=" + enabled +
                ", online=" + online +
                ", canAddPlayers=" + canAddPlayers +
                ", globalServerInfo=" + globalServerInfo +
                ", players=" + players +
                '}';
    }
}
