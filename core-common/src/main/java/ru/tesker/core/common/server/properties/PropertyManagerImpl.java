package ru.tesker.core.common.server.properties;

import org.springframework.stereotype.Component;
import ru.tesker.core.api.server.properties.PropertyManager;
import ru.tesker.core.api.server.properties.PropertyQuery;
import ru.tesker.core.api.server.properties.ServerProperty;

@Component
public class PropertyManagerImpl implements PropertyManager {

    @Override
    public <T> ServerProperty<T> createProperty(PropertyQuery<T> template, T value) {
        return new ServerPropertyImpl<>(template.getId(), value);
    }

    @Override
    public <T> PropertyQuery<T> queryFrom(String id, Class<T> clazz) {
        return new PropertyQueryImpl<>(id, clazz);
    }

}
