package ru.tesker.core.common.translation;

import ru.tesker.core.api.translation.Translation;
import ru.tesker.core.api.translation.TranslationContext;

import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class TranslationI18N implements Translation {

    private String contextId;
    private Map<Locale, String> locales = new HashMap<>();
    private String id;

    public TranslationI18N(String contextId, Map<Locale, String> locales, String id) {
        this.contextId = contextId;
        this.locales = locales;
        this.id = id;
    }

    public TranslationI18N(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String get(Locale locale) {
        return locales.getOrDefault(locale, contextId + "|" + id);
    }

    @Override
    public Map<Locale, String> getTranslations() {
        return Collections.unmodifiableMap(new HashMap<>(locales));
    }

    @Override
    public void set(String s, Locale locale) {
        locales.put(locale, s);
    }
}
