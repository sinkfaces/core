package ru.tesker.core.common;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import io.nats.client.Connection;
import io.nats.client.Nats;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.tesker.core.api.PersistenceManager;

import java.io.IOException;


@Configuration
@ComponentScan(basePackages = "ru.tesker.core.common")
public class CommonCoreSpringConfig {

    @Bean
    public Cluster cluster() {
        return Cluster.builder().addContactPoint("127.0.0.1")
                .withCredentials("cassandra", "cassandra")
                .build();
    }

    @Bean
    public Connection connection() {
        try {
            return Nats.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Bean
    @Autowired
    public Session session(PersistenceManager persistenceManager) {
        return persistenceManager.getSession();
    }

}
