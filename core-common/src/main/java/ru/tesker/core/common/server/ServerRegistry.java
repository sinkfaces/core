package ru.tesker.core.common.server;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.mapping.Mapper;
import com.google.common.collect.ImmutableSet;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tesker.core.api.PersistenceManager;
import ru.tesker.core.api.bungee.ServerSpecDTO;
import ru.tesker.core.api.bungee.ServerSpecification;
import ru.tesker.core.api.server.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class ServerRegistry {

    Set<ServerDataImpl> allServers = Collections.newSetFromMap(new ConcurrentHashMap<ServerDataImpl, Boolean>());
    Set<ServerDataImpl> servers = Collections.newSetFromMap(new ConcurrentHashMap<ServerDataImpl, Boolean>());
    private ServerImpl currentServer;

    private PersistenceManager persistenceManager;

    public ListenableFuture<ServerImpl> registerServer(ServerType serverType, String gameName, ServerDataProvider serverDataProvider) {
        Optional<ServerImpl> currentServerOpt = getCurrentServer();
        if(!currentServerOpt.isPresent()) {
            DefaultGlobalServerInfo globalServerInfo = DefaultGlobalServerInfo
                    .builder()
                    .setServerType(serverType)
                    .setGameName(gameName)
                    .createDefaultGlobalServerInfo();
            Mapper<ServerDataImpl> mapper = persistenceManager.getMappingManager().mapper(ServerDataImpl.class);
            ServerDataImpl serverData = mapper.get(serverDataProvider.getIp().getAddress(), serverDataProvider.getIp().getPort());
            if(serverData != null) {
                this.currentServer = new ServerImpl(serverDataProvider, globalServerInfo, ServerDataUtils.getPosition(serverData.getId()));
                return Futures.immediateFuture(currentServer);
            } else {
                ServerAccessor serverAccessor = persistenceManager.getMappingManager().createAccessor(ServerAccessor.class);
                ListenableFuture<ResultSet> position = serverAccessor.countServersAsync(serverType, gameName);
                ListenableFuture<ServerImpl> serverImplFuture = Futures.transform(position,
                        resultSet -> new ServerImpl(serverDataProvider, globalServerInfo, (int) resultSet.one().getLong("count")),
                        MoreExecutors.directExecutor());
                Futures.addCallback(serverImplFuture, new FutureCallback<ServerImpl>() {
                    @Override
                    public void onSuccess(ServerImpl server) {
                        servers.add(server.getServerData());
                        currentServer = server;
                        mapper.saveAsync(server.getServerData());
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }, MoreExecutors.directExecutor());
                return serverImplFuture;
            }
        }
        throw new IllegalArgumentException("Server has been already registered");
    }


    public Set<ServerDataImpl> getAllServers() {
        return ImmutableSet.copyOf(allServers);
    }

    public Optional<ServerImpl> getCurrentServer() {
        return Optional.ofNullable(currentServer);
    }

    public Collection<ServerDataImpl> getServers() {
        return ImmutableSet.copyOf(servers);
    }

    public Collection<ServerData> getServers(ServerSpecification serverSpecification) {
        return servers
                .stream()
                .filter(server -> serverSpecification.doesFit(new ServerSpecDTO(server)))
                .collect(ImmutableSet.toImmutableSet());
    }

    @Autowired
    public void setPersistenceManager(PersistenceManager persistenceManager) {
        this.persistenceManager = persistenceManager;
    }

}
