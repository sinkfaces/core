package ru.tesker.core.common;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.CodecRegistry;
import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.extras.codecs.enums.EnumNameCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.tesker.core.api.CoreAPI;
import ru.tesker.core.api.codec.LocaleCodec;
import ru.tesker.core.api.scheduler.Scheduler;
import ru.tesker.core.api.server.ServerType;
import ru.tesker.core.common.config.CassandraConfig;
import ru.tesker.core.common.server.ServerUpdater;

@Component
public class CoreCommonPlugin {

    private ServerUpdater serverUpdater;

    private static CoreCommonPlugin instance;

    private Logger logger = LoggerFactory.getLogger(CoreCommonPlugin.class);

    public CoreCommonPlugin(ApplicationContext applicationContext, Scheduler scheduler, CassandraConfig cassandraConfig) {
        if(instance == null) {
            instance = this;
            Cluster cluster = Cluster.builder()
                    .addContactPoint(cassandraConfig.getHost())
                    .withCredentials(cassandraConfig.getUser(), cassandraConfig.getPass())
                    .build();

            Metadata clusterMetadata = cluster.getMetadata();
            KeyspaceMetadata keyspaceMetadata = clusterMetadata.getKeyspace("mixmine");
            CodecRegistry.DEFAULT_INSTANCE.register(new EnumNameCodec<>(ServerType.class), new LocaleCodec());

            logger.info("registering serverUpdater");
            serverUpdater = applicationContext.getBean(ServerUpdater.class);
            CoreApiCreator bean = applicationContext.getBean(CoreApiCreator.class);
            bean.create();
            scheduler.runTaskTimerAsync(serverUpdater, 0, 20);
        }
    }

    public void stop() {
        CoreAPI.getPersistenceManager().getCluster().close();
        CoreAPI.getNatsConnection().close();
    }


    public static ServerUpdater getServerUpdater() {
        return instance.serverUpdater;
    }

}
