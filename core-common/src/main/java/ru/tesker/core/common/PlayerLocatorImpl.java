package ru.tesker.core.common;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tesker.core.api.PlayerLocator;
import ru.tesker.core.api.bungee.BungeeManager;
import ru.tesker.core.api.bungee.FullyGlobalServerInfoSpecification;
import ru.tesker.core.api.bungee.ServerCanAcceptPlayersSpecification;
import ru.tesker.core.api.bungee.ServerSpecification;
import ru.tesker.core.api.entities.SharedPlayer;
import ru.tesker.core.api.server.GlobalServerInfo;

@Component
public class PlayerLocatorImpl implements PlayerLocator {

    private BungeeManager bungeeManager;

    @Autowired
    public PlayerLocatorImpl(BungeeManager bungeeManager) {
        this.bungeeManager = bungeeManager;
    }

    @Override
    public ListenableFuture<Boolean> sendToServer(SharedPlayer sharedPlayer, GlobalServerInfo server) {
        ServerSpecification spec = ServerSpecification.combiner()
                .addServerSpecification(new FullyGlobalServerInfoSpecification(server))
                .addServerSpecification(new ServerCanAcceptPlayersSpecification())
                .create();
        return Futures.immediateFuture(true);
    }

    @Override
    public ListenableFuture<Boolean> sendToCurrentGameLobby(SharedPlayer sharedPlayer) {
        return Futures.immediateFuture(true);
    }

}
