package ru.tesker.core.common;

import io.nats.client.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tesker.core.api.CoreAPI;
import ru.tesker.core.api.PersistenceManager;
import ru.tesker.core.api.PlayerLocator;
import ru.tesker.core.api.bungee.BungeeManager;
import ru.tesker.core.api.scheduler.Scheduler;
import ru.tesker.core.api.server.properties.PropertyManager;
import ru.tesker.core.api.translation.TranslationManager;

@Component
public class CoreApiCreator {

    private PlayerLocator playerLocator;
    private BungeeManager bungeeManager;
    private TranslationManager translationManager;
    private PersistenceManager persistenceManager;
    private Scheduler scheduler;
    private Connection natsConnection;
    private PropertyManager propertyManager;

    private Logger logger = LoggerFactory.getLogger(CoreApiCreator.class);

    @Autowired
    public CoreApiCreator(PlayerLocator playerLocator,
                          BungeeManager bungeeManager,
                          TranslationManager translationManager,
                          PersistenceManager persistenceManager,
                          Connection natsConnection,
                          Scheduler scheduler,
                          PropertyManager propertyManager) {
        this.playerLocator = playerLocator;
        this.natsConnection = natsConnection;
        this.bungeeManager = bungeeManager;
        this.translationManager = translationManager;
        this.persistenceManager = persistenceManager;
        this.scheduler = scheduler;
        this.propertyManager = propertyManager;
    }

    void create() {
        new CoreAPI(playerLocator, bungeeManager, translationManager, persistenceManager, natsConnection, scheduler, propertyManager);
    }

}
