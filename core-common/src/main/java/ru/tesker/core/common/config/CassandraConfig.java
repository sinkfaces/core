package ru.tesker.core.common.config;

/**
 * TheSeems Code.
 */

public class CassandraConfig {

    private String host;
    private String user;
    private String pass;

    public String getHost() {
        return host;
    }

    public String getPass() {
        return pass;
    }

    public String getUser() {
        return user;
    }
}
