package ru.tesker.core.common.entities;

import com.datastax.driver.mapping.annotations.Field;
import com.datastax.driver.mapping.annotations.UDT;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import ru.tesker.core.api.entities.SharedPlayer;
import ru.tesker.core.api.server.ServerData;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.UUID;

@UDT(keyspace = "mixmine", name = "sharedplayer")
public class SharedPlayerImpl implements SharedPlayer {

    private InetAddress ip;
    private int port;

    @Field(name = "uniqueid")
    private UUID uniqueId;

    private ServerData currentServer;

    public SharedPlayerImpl() {}

    private SharedPlayerImpl(InetSocketAddress ip, UUID uuid, ServerData currentServer) {
        this.ip = ip.getAddress();
        this.port = ip.getPort();
        this.uniqueId = uuid;
        this.currentServer = currentServer;
    }

    public void setIp(InetAddress ip) {
        this.ip = ip;
    }

    public void setPort(int port) {
        this.port = port;
    }

//    @Transient
//    @Override
//    public InetSocketAddress getInetSocketAddress() {
//        return new InetSocketAddress(ip, port);
//    }

    @Override
    public UUID getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }

    @Override
    public ServerData getCurrentServer() {
        return currentServer;
    }

    public void setCurrentServer(ServerData currentServer) {
        this.currentServer = currentServer;
    }

    public static SharedPlayerImpl createSharedPlayer(InetSocketAddress ip, UUID uuid) {
        return new SharedPlayerImpl(ip, uuid, null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        SharedPlayerImpl that = (SharedPlayerImpl) o;

        return new EqualsBuilder()
                .append(getUniqueId(), that.getUniqueId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getUniqueId())
                .toHashCode();
    }
}
