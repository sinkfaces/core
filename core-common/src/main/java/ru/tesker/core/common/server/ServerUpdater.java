package ru.tesker.core.common.server;

import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.Result;
import com.google.common.collect.ImmutableSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tesker.core.api.PersistenceManager;
import ru.tesker.core.api.server.ServerData;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

@Component
public class ServerUpdater implements Runnable {

    private ServerRegistry serverRegistry;
    private ServerAccessor serverAccessor;
    private Mapper<ServerDataImpl> mapper;

    private Logger logger = LoggerFactory.getLogger(ServerUpdater.class);

    private List<Consumer<Collection<ServerData>>> consumers = new CopyOnWriteArrayList<>();

    @Autowired
    public ServerUpdater(ServerRegistry serverRegistry, PersistenceManager persistenceManager) {
        this.serverRegistry = serverRegistry;
        this.serverAccessor = persistenceManager.getMappingManager().createAccessor(ServerAccessor.class);
        this.mapper = persistenceManager.getMappingManager().mapper(ServerDataImpl.class);
    }

    @Override
    public void run() {
        Optional<ServerImpl> currentServerOpt = serverRegistry.getCurrentServer();
        if(currentServerOpt.isPresent()) {
            ServerImpl server = currentServerOpt.get();
            ServerDataImpl serverData = server.getServerData();
            mapper.save(serverData);
            serverAccessor.setOnlineWithTtl(serverData.getAddress(), serverData.getPort(), 5);
        }
        Result<ServerDataImpl> serversTemp = serverAccessor.getAll();
        Set<ServerDataImpl> newServerData = new HashSet<>();
        for(ServerDataImpl serverData : serversTemp) {
            if(serverData.isOnline()) {
                newServerData.add(serverData);
            }
        }
        serverRegistry.servers = newServerData;
        consumers.forEach(consumer -> consumer.accept(ImmutableSet.copyOf(newServerData)));
    }

    public void addListener(Consumer<Collection<ServerData>> consumer) {
        consumers.add(consumer);
    }

}
