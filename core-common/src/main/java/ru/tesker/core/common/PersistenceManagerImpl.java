package ru.tesker.core.common;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.MappingManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tesker.core.api.PersistenceManager;

@Component
public class PersistenceManagerImpl implements PersistenceManager {

    private final Cluster cluster;
    private final Session session;
    private final MappingManager mappingManager;

    @Autowired
    public PersistenceManagerImpl(Cluster cluster) {
        this.cluster = cluster;
        this.session = cluster.connect();
        this.mappingManager = new MappingManager(session);
    }

    @Override
    public Cluster getCluster() {
        return cluster;
    }

    @Override
    public Session getSession() {
        return session;
    }

    @Override
    public MappingManager getMappingManager() {
        return mappingManager;
    }

}
