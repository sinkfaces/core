package ru.tesker.core.common.server.properties;

import ru.tesker.core.api.server.properties.PropertyQuery;

public class PropertyQueryImpl<T> implements PropertyQuery<T> {

    private String id;
    private Class<T> type;

    PropertyQueryImpl(String id, Class<T> type) {
        this.id = id;
        this.type = type;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Class<T> getType() {
        return type;
    }

}
