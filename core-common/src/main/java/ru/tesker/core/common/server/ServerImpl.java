package ru.tesker.core.common.server;

import ru.tesker.core.api.server.DefaultGlobalServerInfo;
import ru.tesker.core.api.server.GlobalServerInfo;
import ru.tesker.core.api.server.Server;
import ru.tesker.core.api.server.ServerDataProvider;
import ru.tesker.core.api.server.properties.ServerProperty;
import ru.tesker.core.common.entities.SharedPlayerImpl;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.*;

public class ServerImpl implements Server {

    private String id;
    private InetAddress address;
    private int port;

    private final ServerDataProvider serverDataProvider;

    private DefaultGlobalServerInfo globalServerInfo;
    private Collection<SharedPlayerImpl> players = new HashSet<>();

    private Map<String, ServerProperty<String>> serverPropertyMap = new HashMap<>();

    public ServerImpl(ServerDataProvider serverDataProvider, DefaultGlobalServerInfo globalServerInfo, int position) {
        this.id = createServerId(globalServerInfo, position);
        this.serverDataProvider = serverDataProvider;
        this.globalServerInfo = globalServerInfo;

        InetSocketAddress ip = serverDataProvider.getIp();
        this.address = ip.getAddress();
        this.port = ip.getPort();
    }

    private static String createServerId(GlobalServerInfo globalServerInfo, int position) {
        StringJoiner joiner = new StringJoiner(":");
        joiner.add(globalServerInfo.getServerType().toString());
        joiner.add(globalServerInfo.getGameName());
        joiner.add(String.valueOf(position));

        return joiner.toString();
    }


    @Override
    public String getId() {
        return id;
    }

    @Override
    public InetSocketAddress getAddress() {
        return new InetSocketAddress(address, port);
    }

    @Override
    public Collection<SharedPlayerImpl> getPlayers() {
        return Collections.unmodifiableCollection(new HashSet<>(players));
    }

    @Override
    public GlobalServerInfo getGlobalServerInfo() {
        return globalServerInfo;
    }


    @Override
    public void addServerProperty(ServerProperty<String> serverProperty) {
        serverPropertyMap.put(serverProperty.getId(), serverProperty);
    }

    @Override
    public ServerDataImpl getServerData() {
        Map<String, String> tmp = new HashMap<>();
        serverPropertyMap.forEach((k, v) -> tmp.put(k, v.getValue()));

        return new ServerDataImpl(id, serverDataProvider, globalServerInfo, players, tmp);
    }


}
