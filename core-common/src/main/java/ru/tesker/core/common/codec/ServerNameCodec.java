package ru.tesker.core.common.codec;

import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.extras.codecs.MappingCodec;
import ru.tesker.core.api.CoreAPI;
import ru.tesker.core.api.bungee.ServerNameSpecification;
import ru.tesker.core.api.server.ServerData;

public class ServerNameCodec extends MappingCodec<ServerData, String> {

    public ServerNameCodec() {
        super(TypeCodec.varchar(), ServerData.class);
    }

    @Override
    protected ServerData deserialize(String serverId) {
        if(!CoreAPI.isInitialized()) {
            return null;
        }
        return CoreAPI.getBungeeManager().getServers(new ServerNameSpecification(serverId)).stream().findAny().orElse(null);
    }

    @Override
    protected String serialize(ServerData server) {
        if(server == null) {
            return "unknown";
        }
        return server.getId();
    }

}
