package ru.tesker.core.common.translation.dao;

import com.datastax.driver.core.*;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tesker.core.api.PersistenceManager;
import ru.tesker.core.api.translation.Translation;
import ru.tesker.core.api.translation.TranslationContext;
import ru.tesker.core.common.translation.TranslationContextImpl;
import ru.tesker.core.common.translation.TranslationI18N;

import java.util.*;

@Component
public class CassandraTranslationContextDAO implements TranslationContextDAO {

    private Session session;
    private static final String KEYSPACE = "mixmine";
    private static final String TABLE = "translations";

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(CassandraTranslationContextDAO.class);

    @Autowired
    public CassandraTranslationContextDAO(PersistenceManager persistenceManager) {
        this.session = persistenceManager.getSession();
    }


    @Override
    public ListenableFuture<Void> save(TranslationContext translationContext) {
        BatchStatement batchStatement = new BatchStatement();
        for(Translation translation : translationContext.getTranslations()) {
            Statement statement = QueryBuilder.insertInto(KEYSPACE, TABLE)
                    .value("context_id", translationContext.getId())
                    .value("translation_id", translation.getId())
                    .value("translations", translation.getTranslations());
            LOGGER.info("Saving translation context, query: " + statement.toString());
            batchStatement.add(statement);
        }
        return Futures.transform(session.executeAsync(batchStatement), resultSet -> (Void) null, MoreExecutors.directExecutor());
    }

    @Override
    public ListenableFuture<Optional<TranslationContext>> getContext(String id) {
        Statement statement = QueryBuilder.select().all().from(KEYSPACE, TABLE);//.where(QueryBuilder.eq("context_id", id));
        ListenableFuture<ResultSet> resultSetFuture = session.executeAsync(statement);
        return Futures.transform(resultSetFuture, resultSet -> mapContext(id, resultSet), MoreExecutors.directExecutor());
    }

    private Optional<TranslationContext> mapContext(String id, ResultSet resultSet) {
        Map<String, TranslationI18N> translations = new HashMap<>();
        List<Row> all = resultSet.all();
        if(all.size() == 0) return Optional.empty();
        for(Row row : all) {
            TranslationI18N translationI18N = map(id, row);
            translations.put(translationI18N.getId(), translationI18N);
        }
        return Optional.of(new TranslationContextImpl(id, translations));
    }

    private TranslationI18N map(String contextId, Row row) {
        String id = row.getString("translation_id");
        Map<Locale, String> translations = row.getMap("translations", Locale.class, String.class);
        return new TranslationI18N(contextId, translations, id);
    }
}
