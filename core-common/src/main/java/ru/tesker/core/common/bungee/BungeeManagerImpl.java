package ru.tesker.core.common.bungee;

import com.google.common.util.concurrent.ListenableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tesker.core.api.bungee.BungeeManager;
import ru.tesker.core.api.bungee.ServerSpecification;
import ru.tesker.core.api.server.ServerData;
import ru.tesker.core.api.server.ServerDataProvider;
import ru.tesker.core.api.server.ServerType;
import ru.tesker.core.common.server.ServerDataImpl;
import ru.tesker.core.common.server.ServerImpl;
import ru.tesker.core.common.server.ServerRegistry;

import java.util.Collection;
import java.util.Optional;

@Component
public class BungeeManagerImpl implements BungeeManager {

    private ServerRegistry serverRegistry;


    @Override
    public ListenableFuture<ServerImpl> registerServer(ServerType serverType, String gameName, ServerDataProvider serverDataProvider) {
        return serverRegistry.registerServer(serverType, gameName, serverDataProvider);
    }

    @Override
    public Optional<ServerImpl> getCurrentServer() {
        return serverRegistry.getCurrentServer();
    }


    @Override
    public Collection<ServerData> getServers(ServerSpecification serverSpecification) {
        return serverRegistry.getServers(serverSpecification);
    }

    @Override
    public Collection<ServerDataImpl> getServers() {
        return serverRegistry.getServers();
    }

    @Autowired
    public void setServerRegistry(ServerRegistry serverRegistry) {
        this.serverRegistry = serverRegistry;
    }
}
