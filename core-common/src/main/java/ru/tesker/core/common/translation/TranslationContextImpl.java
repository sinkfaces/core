package ru.tesker.core.common.translation;

import ru.tesker.core.api.translation.Translation;
import ru.tesker.core.api.translation.TranslationContext;

import java.util.*;

public class TranslationContextImpl implements TranslationContext {

    private String id;
    private Map<String, TranslationI18N> translations = new HashMap<>();

    public TranslationContextImpl(String id) {
        this.id = id;
    }

    public TranslationContextImpl(String id, Map<String, TranslationI18N> translations) {
        this.id = id;
        this.translations = translations;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Collection<Translation> getTranslations() {
        return Collections.unmodifiableCollection(new HashSet<>(translations.values()));
    }

    @Override
    public Optional<Translation> getTranslation(String id) {
        return Optional.ofNullable(translations.get(id));
    }

    @Override
    public Translation createTranslation(String id) {
        if(translations.containsKey(id)) {
            throw new IllegalArgumentException(String.format("Translation %s already exists", id));
        }
        TranslationI18N translationI18N = new TranslationI18N(id);
        translations.put(id, translationI18N);
        return translationI18N;
    }
}
