package ru.tesker.core.common.server;

import ru.tesker.core.api.server.ServerData;
import ru.tesker.core.api.server.ServerDataUtils;

import java.util.Comparator;

public class ServerPositionComparator implements Comparator<ServerData> {
    @Override
    public int compare(ServerData o1, ServerData o2) {
        return Integer.compare(ServerDataUtils.getPosition(o1.getId()), ServerDataUtils.getPosition(o2.getId()));
    }
}
