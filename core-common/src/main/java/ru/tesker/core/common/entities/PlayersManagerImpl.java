package ru.tesker.core.common.entities;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import io.nats.client.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tesker.core.api.entities.PlayersManager;
import ru.tesker.core.api.entities.SharedPlayer;
import ru.tesker.core.api.utils.MqUtils;
import ru.tesker.core.api.utils.NatsMessagesIds;

import java.io.IOException;

public class PlayersManagerImpl implements PlayersManager {

    @Autowired
    private Connection natsConnection;

    @Override
    public void sendMessage(SharedPlayer sharedPlayer, String message) {
        ByteArrayDataOutput bado = ByteStreams.newDataOutput();
        MqUtils.writeUuid(sharedPlayer.getUniqueId(), bado);
        bado.writeUTF(message);
        try {
            natsConnection.publish(NatsMessagesIds.SEND_MESSAGE, bado.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
