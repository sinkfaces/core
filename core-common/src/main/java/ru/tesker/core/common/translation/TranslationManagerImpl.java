package ru.tesker.core.common.translation;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tesker.core.api.translation.TranslationContext;
import ru.tesker.core.api.translation.TranslationManager;
import ru.tesker.core.common.translation.dao.TranslationContextDAO;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class TranslationManagerImpl implements TranslationManager {

    private TranslationContextDAO translationContextDAO;

    private Map<String, TranslationContext> contexts = new HashMap<>();

    @Autowired
    public TranslationManagerImpl(TranslationContextDAO translationContextDAO) {
        this.translationContextDAO = translationContextDAO;
    }

    @Override
    public ListenableFuture<Optional<TranslationContext>> getTranslationContext(String id) {
        if(contexts.containsKey(id)) {
            TranslationContext translationContext = contexts.get(id);
            return Futures.immediateFuture(Optional.ofNullable(translationContext));
        }
        ListenableFuture<Optional<TranslationContext>> contextFuture = translationContextDAO.getContext(id);
        Futures.addCallback(contextFuture, new FutureCallback<Optional<TranslationContext>>() {
            @Override
            public void onSuccess(Optional<TranslationContext> translationContextOpt) {
                translationContextOpt.ifPresent(translationContext -> contexts.put(id, translationContext));
            }

            @Override
            public void onFailure(Throwable throwable) {
                throwable.printStackTrace();
            }
        });
        return contextFuture;
    }

    @Override
    public TranslationContext createTranslationContext(String id) {
        if(contexts.containsKey(id)) {
            throw new IllegalArgumentException(String.format("TranslationContext %s already exists", id));
        }
        TranslationContext translationContext = new TranslationContextImpl(id);
        contexts.put(id, translationContext);
        return translationContext;
    }

    @Override
    public ListenableFuture<Void> save(TranslationContext translationContext) {
        return translationContextDAO.save(translationContext);
    }

}
