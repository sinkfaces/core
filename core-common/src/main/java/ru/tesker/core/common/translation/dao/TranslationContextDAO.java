package ru.tesker.core.common.translation.dao;

import com.google.common.util.concurrent.ListenableFuture;
import ru.tesker.core.api.translation.TranslationContext;

import java.util.Optional;

public interface TranslationContextDAO {

    ListenableFuture<Void> save(TranslationContext translationContext);

    ListenableFuture<Optional<TranslationContext>> getContext(String id);

}
