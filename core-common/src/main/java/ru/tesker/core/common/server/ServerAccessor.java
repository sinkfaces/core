package ru.tesker.core.common.server;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Param;
import com.datastax.driver.mapping.annotations.Query;
import com.google.common.util.concurrent.ListenableFuture;
import ru.tesker.core.api.server.ServerType;

import java.net.InetAddress;

@Accessor
public interface ServerAccessor {

    @Query("select * from mixmine.servers")
    ListenableFuture<Result<ServerDataImpl>> getAllAsync();

    @Query("select * from mixmine.servers")
    Result<ServerDataImpl> getAll();


    @Query("select count(*) from mixmine.servers where globalserverinfo = {servertype: :servertype, gamename: :gamename} allow filtering;")
    ResultSetFuture countServersAsync(@Param("servertype") ServerType serverType,
                                      @Param("gamename") String gameName);

    @Query("select count(*) from mixmine.servers where globalserverinfo = {servertype: :servertype, gamename: :gamename} allow filtering;")
    ResultSet countServers(@Param("servertype") ServerType serverType,
                           @Param("gamename") String gameName);

    @Query("UPDATE mixmine.servers using TTL :ttl set online=true where address=:address and port=:port")
    void setOnlineWithTtl(@Param("address") InetAddress address, @Param("port") int port, @Param("ttl") int ttl);

}
