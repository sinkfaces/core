package ru.tesker.core.common.test.api;

import static org.junit.Assert.*;
import org.junit.Test;
import ru.tesker.core.api.server.ServerData;
import ru.tesker.core.api.server.ServerDataUtils;
import ru.tesker.core.api.server.ServerType;

public class ServerSpecificationTest {

    @Test
    public void check() {
        String serverId = "GAME:skywars:0";
        assertEquals(ServerType.GAME, ServerDataUtils.getType(serverId));
        assertEquals("skywars", ServerDataUtils.getGameName(serverId));
        assertEquals(0, ServerDataUtils.getPosition(serverId));
    }


}
