package ru.tesker.core.examples;

import com.google.common.util.concurrent.ListenableFuture;
import ru.tesker.core.api.entities.PlayersManager;
import ru.tesker.core.api.entities.SharedPlayer;
import ru.tesker.core.api.translation.DefaultLocales;
import ru.tesker.core.api.translation.Translation;
import ru.tesker.core.api.translation.TranslationContext;
import ru.tesker.core.api.translation.TranslationManager;

import java.util.concurrent.ExecutionException;

public class TranslationExample {

    private TranslationManager translationManager;
    private PlayersManager playersManager;

    public TranslationExample(TranslationManager translationManager, PlayersManager playersManager) {
        this.translationManager = translationManager;
        this.playersManager = playersManager;
    }

    public void sendTranslation(SharedPlayer sharedPlayer) throws ExecutionException, InterruptedException {
        ListenableFuture<TranslationContext> translationContextFuture = translationManager.getOrCreateTranslationContext("skywars");
        TranslationContext translationContext = translationContextFuture.get();
        Translation playerJoin = translationContext.getOrCreateTranslation("player_join");
        playersManager.sendMessage(sharedPlayer, playerJoin.get(DefaultLocales.RUSSIAN));
    }

}
