package ru.tesker.core.bukkit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tesker.core.api.scheduler.Scheduler;
import ru.tesker.core.common.CommonCoreSpringConfig;
import ru.tesker.core.common.CoreCommonPlugin;

import java.io.File;

public class CoreBukkitPlugin extends JavaPlugin {

    private static CoreBukkitPlugin instance;

    private CoreCommonPlugin coreCommonPlugin;

    @Override
    public void onEnable() {
        instance = this;
        Scheduler scheduler = new BukkitScheduler(this);
        Thread.currentThread().setContextClassLoader(CoreBukkitPlugin.class.getClassLoader());
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.setClassLoader(CoreBukkitPlugin.class.getClassLoader());
        applicationContext.register(CoreBukkitConfig.class);
        applicationContext.register(CommonCoreSpringConfig.class);
        applicationContext.refresh();

        coreCommonPlugin = new CoreCommonPlugin(applicationContext, scheduler);
    }

    @Override
    public void onDisable() {
        coreCommonPlugin.stop();
    }

    public static CoreBukkitPlugin getInstance() {
        return instance;
    }
}
