package ru.tesker.core.bukkit;

import org.bukkit.Bukkit;
import ru.tesker.core.api.scheduler.Scheduler;

public class BukkitScheduler implements Scheduler {

    private CoreBukkitPlugin plugin;

    BukkitScheduler(CoreBukkitPlugin coreBukkitPlugin) {
        this.plugin = coreBukkitPlugin;
    }

    @Override
    public void runTaskTimer(Runnable runnable, int delay, int interval) {
        Bukkit.getScheduler().runTaskTimer(plugin, runnable, delay, interval);
    }

    @Override
    public void runTaskDelay(Runnable runnable, int delay) {
        Bukkit.getScheduler().runTaskLater(plugin, runnable, delay);
    }

    @Override
    public void runSync(Runnable runnable) {
        Bukkit.getScheduler().runTask(plugin, runnable);
    }

    @Override
    public void runTaskTimerAsync(Runnable runnable, int delay, int interval) {
        Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, runnable, delay, interval);
    }

    @Override
    public void runTaskDelayAsync(Runnable runnable, int delay) {
        Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, runnable, delay);
    }

    @Override
    public void runAsync(Runnable runnable) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, runnable);
    }
}
