package ru.tesker.core.bukkit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CoreBukkitConfig {

    @Bean
    public BukkitScheduler bukkitScheduler() {
        return new BukkitScheduler(CoreBukkitPlugin.getInstance());
    }

}
