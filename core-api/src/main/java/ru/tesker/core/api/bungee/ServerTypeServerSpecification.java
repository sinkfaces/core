package ru.tesker.core.api.bungee;

import ru.tesker.core.api.server.ServerType;

public class ServerTypeServerSpecification implements ServerSpecification {

    private ServerType serverType;

    public ServerTypeServerSpecification(ServerType serverType) {
        this.serverType = serverType;
    }

    @Override
    public boolean doesFit(ServerSpecDTO specDTO) {
        return specDTO.getServer().getGlobalServerInfo().getServerType() == serverType;
    }
}
