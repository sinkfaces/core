package ru.tesker.core.api.server.properties;

public interface PropertyManager {

    /**
     * @param template property template
     * @param value property's value
     * @return newly created ServerProperty
     */
    <T> ServerProperty<T> createProperty(PropertyQuery<T> template, T value);

    /**
     * @param id property query id
     * @param clazz T class
     * @return newly created property query
     */
    <T> PropertyQuery<T> queryFrom(String id, Class<T> clazz);

}
