package ru.tesker.core.api.server;

public class DefaultGlobalServerInfoBuilder {
    private ServerType serverType = ServerType.GAME;
    private String gameName = "unknown";

    DefaultGlobalServerInfoBuilder() {
    }

    public DefaultGlobalServerInfoBuilder setServerType(ServerType serverType) {
        this.serverType = serverType;
        return this;
    }

    public DefaultGlobalServerInfoBuilder setGameName(String gameName) {
        this.gameName = gameName;
        return this;
    }

    public DefaultGlobalServerInfo createDefaultGlobalServerInfo() {
        return new DefaultGlobalServerInfo(serverType, gameName);
    }
}