package ru.tesker.core.api.bungee;

import ru.tesker.core.api.server.ServerData;

public class ServerSpecDTO {

    private ServerData server;

    public ServerSpecDTO(ServerData server) {
        this.server = server;
    }

    public ServerData getServer() {
        return server;
    }
}
