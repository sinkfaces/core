package ru.tesker.core.api.server;

import java.net.InetSocketAddress;

public interface ServerDataProvider {

    /**
     * @return server ip
     */
    InetSocketAddress getIp();

    /**
     * @return max players that server can accept
     */
    int getMaxPlayers();

    /**
     * @return true if server is enabled
     */
    boolean isEnabled();

    /**
     * @return true if server can add player
     */
    boolean canAddPlayers();



}
