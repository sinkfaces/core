package ru.tesker.core.api.codec;

import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.extras.codecs.MappingCodec;

import java.util.Locale;

public class LocaleCodec extends MappingCodec<Locale, String> {
    public LocaleCodec() {
        super(TypeCodec.varchar(), Locale.class);
    }

    @Override
    protected Locale deserialize(String tag) {
        return Locale.forLanguageTag(tag);
    }

    @Override
    protected String serialize(Locale locale) {
        return locale.toLanguageTag();
    }
}
