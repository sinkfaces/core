package ru.tesker.core.api.bungee;

import com.google.common.util.concurrent.ListenableFuture;
import ru.tesker.core.api.server.Server;
import ru.tesker.core.api.server.ServerData;
import ru.tesker.core.api.server.ServerDataProvider;
import ru.tesker.core.api.server.ServerType;

import java.util.Collection;
import java.util.Optional;

public interface BungeeManager {

    /**
     * @param serverType ServerType
     * @param gameName gameName
     * @param serverDataProvider server data provider
     * @return server when registered
     */
    ListenableFuture<? extends Server> registerServer(ServerType serverType, String gameName, ServerDataProvider serverDataProvider);

    /**
     * @return current server registration
     */
    Optional<? extends Server> getCurrentServer();

    /**
     * @param serverSpecification server specification filter
     * @return filtered server by server specification
     */
    Collection<? extends ServerData> getServers(ServerSpecification serverSpecification);

    /**
     * @return all registered servers
     */
    Collection<? extends ServerData> getServers();
}
