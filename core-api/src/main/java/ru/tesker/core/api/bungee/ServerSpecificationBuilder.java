package ru.tesker.core.api.bungee;

import java.util.ArrayList;
import java.util.List;

public class ServerSpecificationBuilder {

    private List<ServerSpecification> serverSpecifications = new ArrayList<>();

    public ServerSpecificationBuilder addServerSpecification(ServerSpecification serverSpecification) {
        serverSpecifications.add(serverSpecification);
        return this;
    }

    public ServerSpecificationCombined create() {
        return new ServerSpecificationCombined(serverSpecifications);
    }
}