package ru.tesker.core.api.entities;

import ru.tesker.core.api.server.ServerData;

import java.util.UUID;

public interface SharedPlayer extends SharedEntity {

    //InetSocketAddress getInetSocketAddress();

    UUID getUniqueId();

    ServerData getCurrentServer();

}
