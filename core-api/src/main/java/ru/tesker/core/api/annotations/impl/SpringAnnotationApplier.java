package ru.tesker.core.api.annotations.impl;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.tesker.core.api.annotations.AnnotationApplier;
import ru.tesker.core.api.annotations.EventListener;

import java.util.Map;

@Component
public class SpringAnnotationApplier implements AnnotationApplier {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void registerEventListeners(ApplicationContext applicationContext) {
        JavaPlugin javaPlugin = applicationContext.getBean(JavaPlugin.class);
        Map<String, Object> eventListeners = applicationContext.getBeansWithAnnotation(EventListener.class);
        logger.info("Registering event listeners. Beans to register " + eventListeners.size());
        eventListeners.values().stream()
                .filter(listener -> {
                    logger.info("Iterating over listener " + listener.getClass().getSimpleName() + " is istanceof Listener? " + (listener instanceof Listener));
                    return listener instanceof Listener;
                })
                .map(Listener.class::cast)
                .forEach(listener -> {
                    logger.info("Registered listener " + listener.getClass().getSimpleName());
                    Bukkit.getPluginManager().registerEvents(listener, javaPlugin);
                });
    }

}
