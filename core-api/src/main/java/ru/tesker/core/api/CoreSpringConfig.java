package ru.tesker.core.api;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import ru.tesker.core.api.bungee.BungeeManager;
import ru.tesker.core.api.translation.TranslationManager;

@Configuration
public class CoreSpringConfig {

    @Bean
    @Lazy
    public PlayerLocator playerLocator() {
        return CoreAPI.getPlayerLocator();
    }

    @Bean
    @Lazy
    public PersistenceManager persistenceManager() {
        return CoreAPI.getPersistenceManager();
    }

    @Bean
    @Lazy
    public TranslationManager translationManager() {
        return CoreAPI.getTranslationManager();
    }

    @Bean
    @Lazy
    public BungeeManager bungeeManager() {
        return CoreAPI.getBungeeManager();
    }

}
