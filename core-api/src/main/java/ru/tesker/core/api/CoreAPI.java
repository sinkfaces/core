package ru.tesker.core.api;

import io.nats.client.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.tesker.core.api.bungee.BungeeManager;
import ru.tesker.core.api.scheduler.Scheduler;
import ru.tesker.core.api.server.properties.PropertyManager;
import ru.tesker.core.api.translation.TranslationManager;

public class CoreAPI {

    private static CoreAPI instance;
    private PlayerLocator playerLocator;
    private BungeeManager bungeeManager;
    private TranslationManager translationManager;
    private PersistenceManager persistenceManager;
    private Connection natsConnection;
    private Scheduler scheduler;
    private PropertyManager propertyManager;

    private Logger logger = LoggerFactory.getLogger(CoreAPI.class);

    public CoreAPI(PlayerLocator playerLocator,
                   BungeeManager bungeeManager,
                   TranslationManager translationManager,
                   PersistenceManager persistenceManager,
                   Connection natsConnection,
                   Scheduler scheduler,
                   PropertyManager propertyManager) {
        logger.info("Initializing CoreApi");
        if(instance != null) {
            throw new IllegalArgumentException("CoreAPI is already initialized");
        }
        instance = this;
        this.scheduler = scheduler;
        this.playerLocator = playerLocator;
        this.propertyManager = propertyManager;
        this.natsConnection = natsConnection;
        this.bungeeManager = bungeeManager;
        this.translationManager = translationManager;
        this.persistenceManager = persistenceManager;
    }

    public static PropertyManager getPropertyManager() {
        return getInstance().propertyManager;
    }

    public static Scheduler getScheduler() {
        return getInstance().scheduler;
    }

    public static Connection getNatsConnection() {
        return getInstance().natsConnection;
    }

    public static PersistenceManager getPersistenceManager() {
        return getInstance().persistenceManager;
    }

    public static PlayerLocator getPlayerLocator() {
        return getInstance().playerLocator;
    }

    public static BungeeManager getBungeeManager() {
        return getInstance().bungeeManager;
    }

    public static TranslationManager getTranslationManager() {
        return getInstance().translationManager;
    }

    public static boolean isInitialized() {
        return instance != null;
    }

    private static CoreAPI getInstance() {
        if(instance == null) {
            throw new IllegalArgumentException("CoreAPI hasn't been initialized yet");
        }
        return instance;
    }
}
