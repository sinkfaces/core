package ru.tesker.core.api.server.properties;

public interface ServerProperty<T> {

    String getId();

    T getValue();

}
