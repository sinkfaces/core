package ru.tesker.core.api.annotations;

import org.springframework.context.ApplicationContext;

public interface AnnotationApplier {

    /**
     * Register all beans annotated with @EventListener
     *
     * @param applicationContext application context's beans to register
     */
    void registerEventListeners(ApplicationContext applicationContext);

}
