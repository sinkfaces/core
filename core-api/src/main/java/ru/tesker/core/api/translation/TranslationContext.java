package ru.tesker.core.api.translation;

import java.util.Collection;
import java.util.Optional;

public interface TranslationContext {

    /**
     * @return context id
     */
    String getId();

    /**
     * @return all translations
     */
    Collection<Translation> getTranslations();

    /**
     * @param id translation id
     * @return translation by id
     */
    Optional<Translation> getTranslation(String id);

    /**
     * @param id translation id
     * @return newly created translation
     */
    Translation createTranslation(String id);

    /**
     * @param id translation id
     * @return new translation if there is no suitable one
     */
    default Translation getOrCreateTranslation(String id) {
        return getTranslation(id).orElseGet(() -> createTranslation(id));
    }

}
