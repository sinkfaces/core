package ru.tesker.core.api.scheduler;

public interface Scheduler {

    void runTaskTimer(Runnable runnable, int delay, int interval);

    void runTaskDelay(Runnable runnable, int delay);

    void runSync(Runnable runnable);

    void runTaskTimerAsync(Runnable runnable, int delay, int interval);

    void runTaskDelayAsync(Runnable runnable, int delay);

    void runAsync(Runnable runnable);

}
