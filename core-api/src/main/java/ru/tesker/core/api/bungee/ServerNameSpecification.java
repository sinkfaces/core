package ru.tesker.core.api.bungee;

public class ServerNameSpecification implements ServerSpecification {

    private String id;

    public ServerNameSpecification(String id) {
        this.id = id;
    }

    @Override
    public boolean doesFit(ServerSpecDTO specDTO) {
        return specDTO.getServer().getId().equalsIgnoreCase(id);
    }
}
