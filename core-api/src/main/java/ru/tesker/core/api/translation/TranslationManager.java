package ru.tesker.core.api.translation;

import com.google.common.util.concurrent.AsyncFunction;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import ru.tesker.core.api.concurrent.FutureHelper;

import java.util.Optional;

/**
 * Manager of translations
 */
public interface TranslationManager {

    /**
     * First try to get context from cache if there is no one then try to get it from database
     *
     * @param id context id
     * @return translation context from database
     */
    ListenableFuture<Optional<TranslationContext>> getTranslationContext(String id);


    /**
     * Create a new translation context
     *
     * @param id context id
     * @return a newly created empty translation context
     * @throws IllegalArgumentException if translation context with the same id already exists
     */
    TranslationContext createTranslationContext(String id) throws IllegalArgumentException;

    /**
     * Save translation context into the database
     *
     * @param translation translation context to save
     * @return when it will be saved
     */
    ListenableFuture<Void> save(TranslationContext translation);

    /**
     * @param id context id
     * @return new translation context if there is no suitable one
     */
    default ListenableFuture<TranslationContext> getOrCreateTranslationContext(String id) {
        return FutureHelper.getOrCreate(this::getTranslationContext, this::createTranslationContext, id);
    }
}
