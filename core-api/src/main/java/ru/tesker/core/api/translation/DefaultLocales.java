package ru.tesker.core.api.translation;

import javax.xml.stream.Location;
import java.util.Locale;

public class DefaultLocales {

    public static final Locale RUSSIAN = new Locale("ru", "RU");
    public static final Locale ENGLISH = new Locale("en", "UK");

}
