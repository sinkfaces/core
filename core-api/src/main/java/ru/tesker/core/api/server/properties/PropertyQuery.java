package ru.tesker.core.api.server.properties;

import ru.tesker.core.api.CoreAPI;

public interface PropertyQuery<T> {

    String getId();

    Class<T> getType();

    default ServerProperty<T> newProperty(T value) {
        return CoreAPI.getPropertyManager().createProperty(this, value);
    }

    static <T> PropertyQuery<T> of(String id, Class<T> type) {
        return CoreAPI.getPropertyManager().queryFrom(id, type);
    }

}
