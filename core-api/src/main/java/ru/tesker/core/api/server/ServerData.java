package ru.tesker.core.api.server;

import ru.tesker.core.api.entities.SharedPlayer;

import java.net.InetAddress;
import java.util.Map;
import java.util.Collection;

public interface ServerData {

    /**
     * @return server data id in server system as ServerType:Game_Name:Server_id. For example GAME:bedwars:0
     */
    String getId();

    /**
     * @return server address
     */
    InetAddress getAddress();

    /**
     * @return server port
     */
    int getPort();

    /**
     * @return max players that server can accept
     */
    int getMaxPlayers();

    /**
     * @return true if server is online
     */
    boolean isOnline();

    /**
     * @return true if server is enabled
     */
    boolean isEnabled();

    /**
     * @return true if server can add new players
     */
    boolean isCanAddPlayers();

    /**
     * @return server's global server info
     */
    DefaultGlobalServerInfo getGlobalServerInfo();

    /**
     * @return server players (not implemented yet)
     */
    Collection<? extends SharedPlayer> getPlayers();

    /**
     * @return server properties
     */
    Map<String, String> getProperties();

}
