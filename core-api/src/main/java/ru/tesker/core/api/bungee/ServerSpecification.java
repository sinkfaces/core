package ru.tesker.core.api.bungee;

public interface ServerSpecification {

    /**
     * @param specDTO
     * @return true if server does fit the specification
     */
    boolean doesFit(ServerSpecDTO specDTO);

    static ServerSpecificationBuilder combiner() {
        return new ServerSpecificationBuilder();
    }

}
