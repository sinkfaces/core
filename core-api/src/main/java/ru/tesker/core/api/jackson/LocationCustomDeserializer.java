package ru.tesker.core.api.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.io.IOException;

public class LocationCustomDeserializer extends JsonDeserializer<Location> {

    @Override
    public Location deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        String world = treeNode.get("world").asText();
        double x = treeNode.get("x").asDouble();
        double y = treeNode.get("y").asDouble();
        double z = treeNode.get("z").asDouble();
        return new Location(Bukkit.getWorld(world), x, y, z);
    }

}
