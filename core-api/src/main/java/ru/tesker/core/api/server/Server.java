package ru.tesker.core.api.server;

import ru.tesker.core.api.entities.SharedPlayer;
import ru.tesker.core.api.server.properties.ServerProperty;

import java.net.InetSocketAddress;
import java.util.Collection;

public interface Server {

    /**
     * @return Server id in the server system in format serverType:gameName:0, serverType:gameName:1, serverType:gameName:2
     */
    String getId();

    /**
     * @return Server ip
     */
    InetSocketAddress getAddress();

    Collection<? extends SharedPlayer> getPlayers();

    GlobalServerInfo getGlobalServerInfo();

    ServerData getServerData();

    void addServerProperty(ServerProperty<String> serverProperty);

}
