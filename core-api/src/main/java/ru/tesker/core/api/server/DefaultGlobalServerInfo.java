package ru.tesker.core.api.server;

import com.datastax.driver.mapping.annotations.UDT;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@UDT(keyspace = "mixmine", name = "serverinfo")
public class DefaultGlobalServerInfo implements GlobalServerInfo {

    private ServerType serverType;
    private String gameName;

    private DefaultGlobalServerInfo() {

    }

    DefaultGlobalServerInfo(ServerType serverType, String gameName) {
        this.serverType = serverType;
        this.gameName = gameName;
    }

    @Override
    public ServerType getServerType() {
        return serverType;
    }

    @Override
    public String getGameName() {
        return gameName;
    }

    public static DefaultGlobalServerInfoBuilder builder() {
        return new DefaultGlobalServerInfoBuilder();
    }

    @Override
    public String toString() {
        return "DefaultGlobalServerInfo{" +
                "serverType=" + serverType +
                ", gameName='" + gameName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        DefaultGlobalServerInfo that = (DefaultGlobalServerInfo) o;

        return new EqualsBuilder()
                .append(getServerType(), that.getServerType())
                .append(getGameName(), that.getGameName())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getServerType())
                .append(getGameName())
                .toHashCode();
    }
}
