package ru.tesker.core.api.utils;

public class NatsMessagesIds {

    public static final String SEND_MESSAGE = "core.player.send_message";
    public static final String SAFELY_SEND_PLAYER = "bungee.player.uuid.connect";
    public static final String UNSAFE_SEND_PLAYER = "bungee.player.uuid.connect_unsafe";


}
