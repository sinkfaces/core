package ru.tesker.core.api.bungee;

public class ServerCanAcceptPlayersSpecification implements ServerSpecification {

    @Override
    public boolean doesFit(ServerSpecDTO specDTO) {
        return specDTO.getServer().isCanAddPlayers();
    }
}
