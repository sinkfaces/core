package ru.tesker.core.api.bungee;

import ru.tesker.core.api.server.properties.PropertyQuery;

public class ServerPropertySpecification implements ServerSpecification {

    private String id;
    private String value;

    public ServerPropertySpecification(String id, String value) {
        this.id = id;
        this.value = value;
    }

    @Override
    public boolean doesFit(ServerSpecDTO specDTO) {
        return specDTO.getServer().getProperties().getOrDefault(id, "undefined").equalsIgnoreCase(value);
    }

}
