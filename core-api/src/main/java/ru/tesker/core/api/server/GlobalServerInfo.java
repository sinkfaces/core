package ru.tesker.core.api.server;

/**
 * Describe the global server type, for example all sky wars servers.
 */
public interface GlobalServerInfo {

    ServerType getServerType();

    String getGameName();

}
