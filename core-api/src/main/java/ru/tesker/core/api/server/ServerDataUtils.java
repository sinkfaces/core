package ru.tesker.core.api.server;

public class ServerDataUtils {

    public static int getPosition(String id) {
        String[] splitted = getSeparated(id);
        return Integer.valueOf(splitted[2]);
    }

    public static ServerType getType(String id) {
        return ServerType.valueOf(getSeparated(id)[0]);
    }

    public static String getGameName(String id) {
        return getSeparated(id)[1];
    }

    private static String[] getSeparated(String id) {
        return id.split(":");
    }

}
