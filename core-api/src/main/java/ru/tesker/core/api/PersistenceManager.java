package ru.tesker.core.api;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.MappingManager;

public interface PersistenceManager {

    /**
     * @return cassandra cluster
     */
    Cluster getCluster();

    /**
     * @return cassandra mapping manager
     */
    MappingManager getMappingManager();

    /**
     * @return cassandra session
     */
    Session getSession();
}
