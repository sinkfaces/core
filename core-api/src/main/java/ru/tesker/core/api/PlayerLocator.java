package ru.tesker.core.api;

import com.google.common.util.concurrent.ListenableFuture;
import ru.tesker.core.api.entities.SharedPlayer;
import ru.tesker.core.api.server.GlobalServerInfo;

public interface PlayerLocator {


    /**
     * @param sharedPlayer player to send
     * @param server where we have to send player
     * @return true if found suitable server
     */
    ListenableFuture<Boolean> sendToServer(SharedPlayer sharedPlayer, GlobalServerInfo server);


    /**
     * @param sharedPlayer player to send
     * @return true if lobby is available
     */
    ListenableFuture<Boolean> sendToCurrentGameLobby(SharedPlayer sharedPlayer);


}
