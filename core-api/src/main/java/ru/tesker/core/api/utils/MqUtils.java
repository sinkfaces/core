package ru.tesker.core.api.utils;

import com.google.common.io.ByteArrayDataOutput;

import java.util.UUID;

public class MqUtils {

    public static void writeUuid(UUID uuid, ByteArrayDataOutput bado) {
        bado.writeLong(uuid.getMostSignificantBits());
        bado.writeLong(uuid.getLeastSignificantBits());
    }

}
