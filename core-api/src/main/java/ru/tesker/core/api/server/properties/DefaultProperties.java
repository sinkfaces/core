package ru.tesker.core.api.server.properties;

public class DefaultProperties {

    public static final PropertyQuery<String> MAP = PropertyQuery.of("MAP", String.class);

    public static final PropertyQuery<String> STATE = PropertyQuery.of("STATE", String.class);

    public static final String STATE_RELOADING = "RELOADING";
    public static final String STATE_WAITING = "WAITING";
    public static final String STATE_STARTING = "STARTING";
    public static final String STATE_IN_GAME = "IN_GAME";
    public static final String STATE_ENDING = "ENDING";

}
