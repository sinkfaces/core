package ru.tesker.core.api.bungee;

import java.util.List;

public class ServerSpecificationCombined implements ServerSpecification {

    private List<ServerSpecification> serverSpecifications;


    ServerSpecificationCombined(List<ServerSpecification> serverSpecifications) {
        this.serverSpecifications = serverSpecifications;
    }

    @Override
    public boolean doesFit(ServerSpecDTO specDTO) {
        return serverSpecifications
                .stream()
                .allMatch(serverSpecification -> serverSpecification.doesFit(specDTO));
    }
}
