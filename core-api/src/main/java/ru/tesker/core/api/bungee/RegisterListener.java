package ru.tesker.core.api.bungee;

import ru.tesker.core.api.server.ServerData;

import java.util.function.Consumer;

public interface RegisterListener extends Consumer<ServerData> {
}
