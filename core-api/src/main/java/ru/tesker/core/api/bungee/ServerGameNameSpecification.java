package ru.tesker.core.api.bungee;

import ru.tesker.core.api.server.ServerDataUtils;

public class ServerGameNameSpecification implements ServerSpecification {

    private String gameName;

    public ServerGameNameSpecification(String gameName) {
        this.gameName = gameName;
    }

    @Override
    public boolean doesFit(ServerSpecDTO specDTO) {
        return ServerDataUtils.getGameName(specDTO.getServer().getId()).equalsIgnoreCase(gameName);
    }

}
