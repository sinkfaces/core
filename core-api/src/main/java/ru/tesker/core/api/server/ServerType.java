package ru.tesker.core.api.server;

public enum ServerType {

    LOBBY, GAME, STANDALONE

}
