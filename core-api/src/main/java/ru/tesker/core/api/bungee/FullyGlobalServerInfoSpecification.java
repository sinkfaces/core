package ru.tesker.core.api.bungee;

import ru.tesker.core.api.server.GlobalServerInfo;

public class FullyGlobalServerInfoSpecification implements ServerSpecification {

    private GlobalServerInfo globalServerInfo;

    public FullyGlobalServerInfoSpecification(GlobalServerInfo globalServerInfo) {
        this.globalServerInfo = globalServerInfo;
    }

    @Override
    public boolean doesFit(ServerSpecDTO specDTO) {
        return globalServerInfo.equals(specDTO.getServer().getGlobalServerInfo());
    }
}
