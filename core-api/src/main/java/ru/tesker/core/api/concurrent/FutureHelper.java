package ru.tesker.core.api.concurrent;


import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.*;

import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@SuppressWarnings("unchecked")
public class FutureHelper {

    public static <T, V> ListenableFuture<T> getOrCreateAsync(Function<V, ListenableFuture<Optional<T>>> getter, Function<V, ListenableFuture<T>> factory, V dto) {
        return getOrCreateAsync(getter, factory, dto, MoreExecutors.directExecutor());
    }

    public static <T, V> ListenableFuture<T> getOrCreateAsync(Function<V, ListenableFuture<Optional<T>>> getter, Function<V, ListenableFuture<T>> factory, V dto, Executor executor) {
        ListenableFuture<Optional<T>> future = getter.apply(dto);
        AsyncFunction<Optional<T>, T> asyncFunction = optional -> optional.map(Futures::immediateFuture).orElseGet(() -> factory.apply(dto));
        return Futures.transformAsync(future, asyncFunction, executor);
    }


    public static <T, V> ListenableFuture<T> getOrCreate(Function<V, ListenableFuture<Optional<T>>> getter, Function<V, T> factory, V dto) {
        return getOrCreate(getter, factory, dto, MoreExecutors.directExecutor());
    }

    public static <T, V> ListenableFuture<T> getOrCreate(Function<V, ListenableFuture<Optional<T>>> getter, Function<V, T> factory, V dto, Executor executor) {
        ListenableFuture<Optional<T>> future = getter.apply(dto);
        Function<Optional<T>, T> function = optional -> optional.orElseGet(() -> factory.apply(dto));
        return Futures.transform(future, function::apply, executor);
    }

    public static <T, V> ListenableFuture<T> getOrCreateAsync(Supplier<ListenableFuture<Optional<T>>> getter,
                                                              Supplier<ListenableFuture<T>> factory) {
        return getOrCreateAsync(getter, factory, MoreExecutors.directExecutor());
    }

    public static <T, V> ListenableFuture<T> getOrCreateAsync(Supplier<ListenableFuture<Optional<T>>> getter,
                                                                        Supplier<ListenableFuture<T>> factory, Executor executor) {
        Preconditions.checkNotNull(getter, "Null getter");
        Preconditions.checkNotNull(factory, "Null factory");
        Preconditions.checkNotNull(executor, "Null executor");
        ListenableFuture<Optional<T>> future = getter.get();

        AsyncFunction<Optional<T>, T> asyncFunction = optional -> optional.map(Futures::immediateFuture).orElseGet(factory::get);
        return Futures.transformAsync(future, asyncFunction, executor);
    }

    public static <V> void addCallback(ListenableFuture<V> future, Consumer<V> onSuccess, Consumer<Throwable> onFailure) {
        Preconditions.checkNotNull(future, "future");
        Preconditions.checkNotNull(onSuccess, "onSuccess");
        Preconditions.checkNotNull(onFailure, "onFailure");
        Futures.addCallback(future, new FunctionalFutureCallback<>(onSuccess, onFailure), MoreExecutors.directExecutor());
    }

    private static class FunctionalFutureCallback<V> implements FutureCallback<V> {

        private Consumer<V> onSuccess;
        private Consumer<Throwable> onFailure;

        public FunctionalFutureCallback(Consumer<V> onSuccess, Consumer<Throwable> onFailure) {
            this.onSuccess = onSuccess;
            this.onFailure = onFailure;
        }

        @Override
        public void onSuccess(V v) {
            onSuccess.accept(v);
        }

        @Override
        public void onFailure(Throwable throwable) {
            onFailure.accept(throwable);
        }
    }




}
