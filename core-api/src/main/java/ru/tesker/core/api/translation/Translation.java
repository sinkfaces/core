package ru.tesker.core.api.translation;

import java.util.Locale;
import java.util.Map;

public interface Translation {

    /**
     * @return translation id
     */
    String getId();

    /**
     * @param locale translation locale
     * @return i18n translation
     */
    String get(Locale locale);

    /**
     * @return all translations
     */
    Map<Locale, String> getTranslations();

    /**
     * @param s i18 translation
     * @param locale language
     */
    void set(String s, Locale locale);

}
