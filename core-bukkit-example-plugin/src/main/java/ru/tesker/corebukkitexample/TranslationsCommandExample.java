package ru.tesker.corebukkitexample;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.tesker.core.api.translation.Translation;
import ru.tesker.core.api.translation.TranslationContext;

import java.util.Locale;


//Example of how translation system works
public class TranslationsCommandExample implements CommandExecutor {

    private TranslationContext translationContext;

    public TranslationsCommandExample(TranslationContext translationContext) {
        this.translationContext = translationContext;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage("Sorry, we need to know your locale");
        }
        Player player = (Player) sender;
        Locale playerLocale = Locale.forLanguageTag(player.getLocale());
        if(args.length == 0) {
            sender.sendMessage("Specify translation id");
            return true;
        }
        String translationId = args[1];
        Translation translation = translationContext.getOrCreateTranslation(translationId);
        sender.sendMessage(translation.get(playerLocale));
        return true;
    }


}
