package ru.tesker.corebukkitexample;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Date;
import java.util.UUID;

//Classes to store in player's statistic should always be immutable
public class MyCustomObject {

    private UUID uuid;
    private int lastPlace;
    //Field should be immutable
    private Date time;

    public MyCustomObject(UUID uuid, int lastPlace, Date time) {
        this.uuid = uuid;
        this.lastPlace = lastPlace;
        this.time = new Date(time.getTime());
    }

    public UUID getUuid() {
        return uuid;
    }

    public int getLastPlace() {
        return lastPlace;
    }

    public Date getTime() {
        //Return new instance to make MyCustomObject immutable
        return new Date(time.getTime());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        MyCustomObject that = (MyCustomObject) o;

        return new EqualsBuilder()
                .append(getUuid(), that.getUuid())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getUuid())
                .toHashCode();
    }

    @Override
    public String toString() {
        return "MyCustomObject{" +
                "uuid=" + uuid +
                ", lastPlace=" + lastPlace +
                ", time=" + time +
                '}';
    }
}
