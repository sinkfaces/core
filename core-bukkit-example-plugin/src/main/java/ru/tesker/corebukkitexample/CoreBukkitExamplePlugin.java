package ru.tesker.corebukkitexample;

import com.google.common.util.concurrent.FluentFuture;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import ru.tesker.core.api.CoreAPI;
import ru.tesker.core.api.concurrent.FutureHelper;
import ru.tesker.core.api.server.Server;
import ru.tesker.core.api.server.ServerType;
import ru.tesker.core.api.translation.TranslationContext;
import ru.tesker.game.api.Game;
import ru.tesker.game.api.GameApi;
import ru.tesker.game.api.achievement.*;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class CoreBukkitExamplePlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        //Register server with ServerType game and game name "bedwars" and specify server data provider - MyServerDataProvider
        ListenableFuture<? extends Server> skywars = CoreAPI.getBungeeManager().registerServer(ServerType.GAME, "bedwars", new MyServerDataProvider());
        Futures.addCallback(skywars, new FutureCallback<Server>() {
            @Override
            public void onSuccess(Server server) {
                getLogger().info(String.format("Successfully registered server %s", server));
            }

            @Override
            public void onFailure(Throwable throwable) {
                throwable.printStackTrace();
            }
        });
        ListenableFuture<TranslationContext> translationContext = CoreAPI.getTranslationManager().getOrCreateTranslationContext("player_commands");
        Futures.addCallback(translationContext, new FutureCallback<TranslationContext>() {
            @Override
            public void onSuccess(TranslationContext translationContext) {
                getCommand("gettranslation").setExecutor(new TranslationsCommandExample(translationContext));
            }

            @Override
            public void onFailure(Throwable throwable) {
                getLogger().warning("Can't register gettranslation command");
                throwable.printStackTrace();
            }
        });
        ListenableFuture<Game> bedwars = GameApi.getGameManager().getOrCreateGame("bedwars");
        FutureHelper.addCallback(bedwars, this::doWithGame, Throwable::printStackTrace);
    }

    private void doWithGame(Game game) {

        AchievementManager achievementManager = game.getAchievementManager();
        AchievementContext context = achievementManager.getOrCreateContext("mode.single");
        AchievementsResolver achievementsResolver = game.getAchievementsResolver();
        ListenableFuture<Achievements> achievementsFuture = achievementsResolver.getAchievements(context, UUID.randomUUID());
        FutureHelper.addCallback(achievementsFuture, (achievements) -> doWithAchievements(game, achievements), Throwable::printStackTrace);
    }

    private void doWithAchievements(Game game, Achievements achievements) {
        AchievementManager achievementManager = game.getAchievementManager();
        AchievementContext context = achievementManager.getOrCreateContext("mode.single");
        AchievementEntry<Integer> kills = context.getOrCreateEntry("kills", Integer.class, 0);
        achievements.setValue(kills, 15);
        AchievementEntry<MyCustomObject> customObjects = context.getOrCreateEntry("custom_objects", MyCustomObject.class, new MyCustomObject(UUID.randomUUID(), 0, new Date(0)));
        achievements.setValue(customObjects, new MyCustomObject(UUID.randomUUID(), 15, new Date(0)));
    }

}
