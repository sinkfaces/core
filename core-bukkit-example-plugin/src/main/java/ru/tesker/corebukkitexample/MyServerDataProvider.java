package ru.tesker.corebukkitexample;

import org.bukkit.Bukkit;
import ru.tesker.core.api.server.ServerDataProvider;

import java.net.InetSocketAddress;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class MyServerDataProvider implements ServerDataProvider {

    @Override
    public InetSocketAddress getIp() {
        return new InetSocketAddress("127.0.0.1", Bukkit.getPort());
    }

    @Override
    public int getMaxPlayers() {
        return Bukkit.getMaxPlayers();
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean canAddPlayers() {
        //Server will accept players if random want them
        return ThreadLocalRandom.current().nextBoolean();
    }

}
