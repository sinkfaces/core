package ru.tesker.bungeewrapper;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ListenerInfo;
import net.md_5.bungee.api.config.ServerInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.tesker.core.api.server.ServerData;

import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.Map;
import java.util.function.Consumer;

@Component
public class BungeeServerWriter implements Consumer<Collection<ServerData>> {

    private Logger logger = LoggerFactory.getLogger(BungeeServerWriter.class);

    @Override
    public void accept(Collection<ServerData> serverDatas) {
        Map<String, ServerInfo> servers = ProxyServer.getInstance().getServers();
        for(ServerData serverData : serverDatas) {
            if(!serverData.isOnline()) {
                if(servers.containsKey(serverData.getId())) {
                    servers.remove(serverData.getId());
                    removeServer(serverData.getId());
                    logger.info(String.format("Server %s has been disabled", serverData.getId()));
                }
                continue;
            }
            ServerInfo serverInfo = servers.get(serverData.getId());
            if(serverInfo == null) {
                serverInfo = constructInfo(serverData);
                servers.put(serverData.getId(), serverInfo);
                //addServer(serverData.getId());
                logger.info(String.format("Found and added server %s", serverData.getId()));
            }
        }
    }



    private static ServerInfo constructInfo(ServerData data) {
        return ProxyServer.getInstance().constructServerInfo(data.getId(), new InetSocketAddress(data.getAddress(), data.getPort()), "", !data.isEnabled());
    }

    private static void removeServer(String serverName) {
        Collection<ListenerInfo> listeners = ProxyServer.getInstance().getConfig().getListeners();
        for (ListenerInfo listener : listeners) {
            listener.getServerPriority().remove(serverName);
        }
    }

    private static void addServer(String serverName) {
        Collection<ListenerInfo> listeners = ProxyServer.getInstance().getConfig().getListeners();
        for (ListenerInfo listener : listeners) {
            listener.getServerPriority().add(serverName);
        }
    }

}
