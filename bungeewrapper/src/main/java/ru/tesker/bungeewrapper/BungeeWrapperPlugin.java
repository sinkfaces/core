package ru.tesker.bungeewrapper;

import io.nats.client.Connection;
import net.md_5.bungee.api.plugin.Plugin;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tesker.bungeewrapper.msgs.SafeSendToServerMessage;
import ru.tesker.bungeewrapper.msgs.SendMsgToPlayerMessage;
import ru.tesker.bungeewrapper.msgs.UnsafeSend;
import ru.tesker.core.api.CoreAPI;
import ru.tesker.core.api.CoreSpringConfig;
import ru.tesker.core.api.utils.NatsMessagesIds;

public class BungeeWrapperPlugin extends Plugin {

    private static BungeeWrapperPlugin instance;

    @Override
    public void onEnable() {
        instance = this;
//        getProxy().getConfig().getServers().clear();
//        for (ListenerInfo listenerInfo : getProxy().getConfig().getListeners()) {
//            listenerInfo.getServerPriority().clear();
//        }
        Thread.currentThread()
                .setContextClassLoader(BungeeWrapperPlugin.class.getClassLoader());

        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();

        applicationContext.setClassLoader(BungeeWrapperPlugin.class.getClassLoader());
        applicationContext.register(CoreSpringConfig.class);
        applicationContext.register(BungeeWrapperConfig.class);
        applicationContext.refresh();

        BungeeWrapperEnabler enabler = applicationContext.getBean(BungeeWrapperEnabler.class);
        enabler.enable();

        Connection natsConnection = CoreAPI.getNatsConnection();
        natsConnection.subscribe(NatsMessagesIds.SAFELY_SEND_PLAYER, new SafeSendToServerMessage());
        natsConnection.subscribe(NatsMessagesIds.SEND_MESSAGE, new SendMsgToPlayerMessage());
        natsConnection.subscribe(NatsMessagesIds.UNSAFE_SEND_PLAYER, new UnsafeSend());
    }

    public static BungeeWrapperPlugin getInstance() {
        return instance;
    }
}
