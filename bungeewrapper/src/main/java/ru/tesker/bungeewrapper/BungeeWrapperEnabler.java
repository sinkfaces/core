package ru.tesker.bungeewrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tesker.core.common.CoreCommonPlugin;

@Component
public class BungeeWrapperEnabler {

    private BungeeServerWriter bungeeServerWriter;

    private Logger logger = LoggerFactory.getLogger(BungeeWrapperEnabler.class);

    @Autowired
    public BungeeWrapperEnabler(BungeeServerWriter bungeeServerWriter) {
        this.bungeeServerWriter = bungeeServerWriter;
    }

    public void enable() {
        logger.info("Added listener");
        CoreCommonPlugin.getServerUpdater().addListener(bungeeServerWriter);
    }


}
