package ru.tesker.bungeewrapper;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "ru.tesker.bungeewrapper")
public class BungeeWrapperConfig {
}
