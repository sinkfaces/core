package ru.tesker.bungeewrapper.msgs;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import io.nats.client.Message;
import io.nats.client.MessageHandler;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class SendToServerMessage implements MessageHandler {

    private Logger logger = LoggerFactory.getLogger(SendToServerMessage.class);

    @Override
    public void onMessage(Message msg) {
        ByteArrayDataInput badi = ByteStreams.newDataInput(msg.getData());
        UUID uuid = new UUID(badi.readLong(), badi.readLong());
        String id = badi.readUTF();
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
        ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo(id);
        player.connect(serverInfo);
        logger.info(String.format("Sent player %s to %s", uuid.toString(), id));
    }


}
