package ru.tesker.bungeewrapper.msgs;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import io.nats.client.Message;
import io.nats.client.MessageHandler;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.tesker.core.api.CoreAPI;
import ru.tesker.core.api.bungee.*;
import ru.tesker.core.api.server.ServerType;
import ru.tesker.core.api.utils.NatsMessagesIds;

import java.lang.reflect.Proxy;
import java.util.UUID;

public class SafeSendToServerMessage implements MessageHandler {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void onMessage(Message msg) {
        ByteArrayDataInput badi = ByteStreams.newDataInput(msg.getData());
        UUID uuid = new UUID(badi.readLong(), badi.readLong());
        ServerType serverType = ServerType.valueOf(badi.readUTF());
        String gameName = badi.readUTF();
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
        System.out.println("Got message " + NatsMessagesIds.SAFELY_SEND_PLAYER + " servertype: " + serverType + " gameName " + gameName + " uuid " + uuid);
        CoreAPI.getBungeeManager().getServers(ServerSpecification
                .combiner()
                .addServerSpecification(new ServerCanAcceptPlayersSpecification())
                .addServerSpecification(new ServerTypeServerSpecification(serverType))
                .addServerSpecification(new ServerGameNameSpecification(gameName))
                .create()
        ).stream().findAny().ifPresent(server -> {
            logger.info(String.format("Safely sent player %s to %s", uuid.toString(), server.getId()));
            player.connect(ProxyServer.getInstance().getServerInfo(server.getId()));
        });
    }

}
