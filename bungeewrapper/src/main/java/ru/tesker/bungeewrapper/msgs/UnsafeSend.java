package ru.tesker.bungeewrapper.msgs;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import io.nats.client.Message;
import io.nats.client.MessageHandler;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import ru.tesker.core.api.CoreAPI;
import ru.tesker.core.api.bungee.ServerCanAcceptPlayersSpecification;
import ru.tesker.core.api.bungee.ServerGameNameSpecification;
import ru.tesker.core.api.bungee.ServerSpecification;
import ru.tesker.core.api.bungee.ServerTypeServerSpecification;
import ru.tesker.core.api.server.ServerType;
import ru.tesker.core.api.utils.NatsMessagesIds;

import java.util.UUID;

public class UnsafeSend implements MessageHandler {
    @Override
    public void onMessage(Message msg) {
        ByteArrayDataInput badi = ByteStreams.newDataInput(msg.getData());
        UUID uuid = new UUID(badi.readLong(), badi.readLong());
        String serverId = badi.readUTF();
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(uuid);
        System.out.println("Got message " + NatsMessagesIds.UNSAFE_SEND_PLAYER + " serverid: " + serverId + " gameName ");
        player.connect(ProxyServer.getInstance().getServerInfo(serverId));
    }
}
