package ru.tesker.bungeewrapper.msgs;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import io.nats.client.Message;
import io.nats.client.MessageHandler;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.UUID;

public class SendMsgToPlayerMessage implements MessageHandler {

    @Override
    public void onMessage(Message msg) {
        ByteArrayDataInput badi = ByteStreams.newDataInput(msg.getData());
        UUID uuid = new UUID(badi.readLong(), badi.readLong());
        String text = badi.readUTF();
        ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(uuid);
        proxiedPlayer.sendMessage(TextComponent.fromLegacyText(text));
    }

}
