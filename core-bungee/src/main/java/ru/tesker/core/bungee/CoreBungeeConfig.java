package ru.tesker.core.bungee;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CoreBungeeConfig {

    @Bean
    public BungeeScheduler scheduler() {
        return new BungeeScheduler(CoreBungeePlugin.getInstance());
    }

}
