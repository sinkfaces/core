package ru.tesker.core.bungee;

import net.md_5.bungee.api.plugin.Plugin;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tesker.core.api.scheduler.Scheduler;
import ru.tesker.core.common.CommonCoreSpringConfig;
import ru.tesker.core.common.CoreCommonPlugin;
import ru.tesker.core.common.config.CassandraConfig;

import java.io.File;

public class CoreBungeePlugin extends Plugin {

    private CoreCommonPlugin coreCommonPlugin;
    private static CoreBungeePlugin instance;

    @Override
    public void onEnable() {
        instance = this;

        CassandraConfig config = UtilMethods.
                parseConfigStrict(new File(getDataFolder(), "config.json"), CassandraConfig.class);

        Scheduler scheduler = new BungeeScheduler(this);
        Thread.currentThread()
                .setContextClassLoader(CoreCommonPlugin.class.getClassLoader());

        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.setClassLoader(CoreCommonPlugin.class.getClassLoader());
//        applicationContext.register(CoreSpringConfig.class);
        applicationContext.register(CoreBungeeConfig.class);
        applicationContext.register(CommonCoreSpringConfig.class);

        applicationContext.refresh();
        coreCommonPlugin = new CoreCommonPlugin(applicationContext, scheduler, config);
    }

    @Override
    public void onDisable() {
        coreCommonPlugin.stop();
    }

    public static CoreBungeePlugin getInstance() {
        return instance;
    }
}
