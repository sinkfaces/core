package ru.tesker.core.bungee;

import net.md_5.bungee.api.ProxyServer;
import ru.tesker.core.api.scheduler.Scheduler;

import java.util.concurrent.TimeUnit;

public class BungeeScheduler implements Scheduler {

    private CoreBungeePlugin coreBungeePlugin;

    public BungeeScheduler(CoreBungeePlugin coreBungeePlugin) {
        this.coreBungeePlugin = coreBungeePlugin;
    }

    @Override
    public void runTaskTimer(Runnable runnable, int delay, int interval) {

    }

    @Override
    public void runTaskDelay(Runnable runnable, int delay) {

    }

    @Override
    public void runSync(Runnable runnable) {

    }

    @Override
    public void runTaskTimerAsync(Runnable runnable, int delay, int interval) {
        ProxyServer.getInstance().getScheduler().schedule(coreBungeePlugin, runnable, delay, interval, TimeUnit.MILLISECONDS);
    }

    @Override
    public void runTaskDelayAsync(Runnable runnable, int delay) {

    }

    @Override
    public void runAsync(Runnable runnable) {
        ProxyServer.getInstance().getScheduler().runAsync(coreBungeePlugin, runnable);
    }
}
