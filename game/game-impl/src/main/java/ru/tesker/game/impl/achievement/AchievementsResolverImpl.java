package ru.tesker.game.impl.achievement;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import org.slf4j.LoggerFactory;
import ru.tesker.game.api.achievement.AchievementContext;
import ru.tesker.game.api.achievement.AchievementEntry;
import ru.tesker.game.api.achievement.Achievements;
import ru.tesker.game.api.achievement.AchievementsResolver;

import java.util.UUID;

public class AchievementsResolverImpl implements AchievementsResolver {

    private Table<UUID, AchievementContext, Achievements> achievementsTable = HashBasedTable.create();


    @Override
    public ListenableFuture<Achievements> getAchievements(AchievementContext context, UUID uuid) {
        Preconditions.checkNotNull(context, "Null context");
        Preconditions.checkNotNull(uuid, "Null uuid");
        Achievements achievements = achievementsTable.get(uuid, context);
        if(achievements == null) {

        }
        return Futures.immediateFuture(achievements);
    }

    @Override
    public void save(Achievements achievements) {

    }

}
