package ru.tesker.game.impl.dao;

import com.google.common.util.concurrent.ListenableFuture;
import ru.tesker.game.api.Game;
import ru.tesker.game.impl.GameImpl;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface GameDao<T extends Game> {

    ListenableFuture<Optional<T>> getGame(String id);

    ListenableFuture<Void> save(T game);


}
