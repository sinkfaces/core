package ru.tesker.game.impl.dao;

import com.datastax.driver.core.Session;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.mapping.Mapper;
import com.google.common.util.concurrent.ListenableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tesker.core.api.CassandraConsts;
import ru.tesker.core.api.CoreAPI;
import ru.tesker.core.api.PersistenceManager;
import ru.tesker.game.impl.GameData;
import ru.tesker.game.impl.GameImpl;

import java.util.Optional;

@Component
public class CassandraGameDao implements GameDao<GameImpl> {

    private PersistenceManager persistenceManager;

    @Autowired
    public CassandraGameDao(PersistenceManager persistenceManager) {
        this.persistenceManager = persistenceManager;
    }

    @Override
    public ListenableFuture<Optional<GameImpl>> getGame(String id) {
        Session session = persistenceManager.getSession();

        return null;
    }

    @Override
    public ListenableFuture<Void> save(GameImpl game) {

        return null;
    }

}
