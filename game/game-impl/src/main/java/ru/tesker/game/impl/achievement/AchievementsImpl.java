package ru.tesker.game.impl.achievement;

import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import ru.tesker.game.api.achievement.AchievementContext;
import ru.tesker.game.api.achievement.AchievementEntry;
import ru.tesker.game.api.achievement.Achievements;

@SuppressWarnings("unchecked")
public class AchievementsImpl implements Achievements {

    private AchievementContext achievementContext;
    private Map<AchievementEntry<?>, Object> values;

    AchievementsImpl(AchievementContext achievementContext, Map<AchievementEntry<?>, Object> values) {
        this.achievementContext = achievementContext;
        this.values = values;
    }

    @Override
    public <T> T getValue(AchievementEntry<T> entry) {
        Object o = values.get(entry);
        if(o == null) {
            return entry.getDefaultValue();
        }
        if(o.getClass() != entry.getClass()) {
            throw new IllegalArgumentException("Invalid entry");
        }
        return (T) o;
    }

    @Override
    public <T> boolean hasValue(AchievementEntry<T> entry) {
        Object o = values.get(entry);
        return o != null && o.getClass() == entry.getClass();
    }

    @Override
    public <T> void setValue(AchievementEntry<T> achievementEntry, T value) {
        values.put(achievementEntry, value);
    }

    @Override
    public AchievementContext getContext() {
        return achievementContext;
    }

    @Override
    public String toString() {
        return "AchievementsImpl{" +
                "achievementContext=" + achievementContext +
                ", values=" + values +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AchievementsImpl that = (AchievementsImpl) o;

        return new EqualsBuilder()
                .append(values, that.values)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(values)
                .toHashCode();
    }



}
