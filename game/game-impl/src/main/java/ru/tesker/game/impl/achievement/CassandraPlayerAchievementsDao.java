package ru.tesker.game.impl.achievement;

import com.datastax.driver.mapping.Mapper;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tesker.core.api.PersistenceManager;
import ru.tesker.game.api.Game;
import ru.tesker.game.api.GameManager;
import ru.tesker.game.api.achievement.AchievementContext;
import ru.tesker.game.api.achievement.AchievementEntry;

import java.util.HashMap;
import java.util.UUID;
import java.util.Map;

@Component
public class CassandraPlayerAchievementsDao implements PlayerAchievementsDao<AchievementsImpl> {

    private GameManager<Game> gameManager;
    private PersistenceManager persistenceManager;
    private Mapper<DataPlayerAchievement> mapper;

    @Autowired
    public CassandraPlayerAchievementsDao(GameManager<Game> gameManager, PersistenceManager persistenceManager) {
        this.gameManager = gameManager;
        this.persistenceManager = persistenceManager;
        this.mapper = persistenceManager.getMappingManager().mapper(DataPlayerAchievement.class);
    }

    @Override
    public ListenableFuture<AchievementsImpl> getAchievements(Game game, UUID uuid, AchievementContext context) {
        ListenableFuture<DataPlayerAchievement> dataPlayerAchievement = mapper.getAsync(game.getId(), uuid, context.getId());

        Futures.transform(dataPlayerAchievement, input -> input.getContextId(), MoreExecutors.directExecutor());
        return null;
    }

    @Override
    public void save(AchievementsImpl achievements) {

    }

}
