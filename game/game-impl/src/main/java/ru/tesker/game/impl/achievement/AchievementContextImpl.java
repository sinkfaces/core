package ru.tesker.game.impl.achievement;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import ru.tesker.game.api.achievement.AchievementContext;

import ru.tesker.game.api.achievement.AchievementEntry;

import java.util.*;


public class AchievementContextImpl implements AchievementContext {

    private String id;
    private Map<String, AchievementEntry<?>> entries = new HashMap<>();

    AchievementContextImpl(String id) {
        this.id = id;
    }

    AchievementContextImpl(String id, Map<String, AchievementEntry<?>> entries) {
        this.id = id;
        this.entries = entries;
    }

    @Override
    public String getId() {
        return id;
    }



    @Override
    public Collection<AchievementEntry<?>> getEntries() {
        return ImmutableSet.copyOf(entries.values());
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> Optional<AchievementEntry<T>> getEntry(String id, Class<T> type) {
        AchievementEntry<?> achievementEntry = entries.get(id);
        if(achievementEntry == null) {
            return Optional.empty();
        }
        if(!type.isAssignableFrom(achievementEntry.getType())) {
            throw new IllegalArgumentException(String.format("Expected type %s but got %s", achievementEntry.getType().getSimpleName(), type.getSimpleName()));
        }
        return Optional.of((AchievementEntry<T>) achievementEntry);
    }

    @Override
    public <T> AchievementEntry<T> createEntry(String id, Class<T> type, T def) {
        AchievementEntry<T> achievementEntry = new AchievementEntryImpl<>(id, type, def);
        entries.put(id, achievementEntry);
        return achievementEntry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AchievementContextImpl that = (AchievementContextImpl) o;

        return new EqualsBuilder()
                .append(getId(), that.getId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .toHashCode();
    }
}
