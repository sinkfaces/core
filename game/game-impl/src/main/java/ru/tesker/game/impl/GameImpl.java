package ru.tesker.game.impl;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import ru.tesker.game.api.Game;
import ru.tesker.game.api.achievement.AchievementManager;
import ru.tesker.game.api.achievement.AchievementsResolver;
import ru.tesker.game.impl.achievement.AchievementManagerImpl;
import ru.tesker.game.impl.dao.GameDao;

public class GameImpl implements Game {

    private String id;
    private AchievementManagerImpl achievementManager = new AchievementManagerImpl();

    public GameImpl(String id) {
        this.id = id;
    }

    public GameImpl(String id, AchievementManagerImpl achievementManager) {
        this.id = id;
        this.achievementManager = achievementManager;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public AchievementManagerImpl getAchievementManager() {
        return achievementManager;
    }

    @Override
    public AchievementsResolver getAchievementsResolver() {
        return null;
    }

    @Override
    public String toString() {
        return "GameImpl{" +
                "id='" + id + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GameImpl game = (GameImpl) o;

        return new EqualsBuilder()
                .append(getId(), game.getId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .toHashCode();
    }
}
