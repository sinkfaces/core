package ru.tesker.game.impl;

import ru.tesker.game.api.purchase.Customer;
import ru.tesker.game.api.purchase.CustomerCart;
import ru.tesker.game.api.Purse;

public class CustomerImpl implements Customer {

    private Purse purse;
    private CustomerCart customerCart;


    @Override
    public Purse getPurse() {
        return purse;
    }

    @Override
    public CustomerCart getPurchases() {
        return customerCart;
    }

}
