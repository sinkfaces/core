package ru.tesker.game.impl.achievement;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import ru.tesker.game.api.achievement.AchievementEntry;
import ru.tesker.game.impl.FullyImmutable;

public class AchievementEntryImpl<T> implements AchievementEntry<T> {

    private String id;
    private Class<T> type;
    private T defaultValue;

    AchievementEntryImpl(String id, Class<T> type, T defaultValue) {
        //Preconditions.checkState(type.getAnnotation(FullyImmutable.class) != null, "Class should be immutable and should be annotated with @FullyImmutable");
        this.id = id;
        this.type = type;
        this.defaultValue = defaultValue;
    }

    @Override
    public Class<T> getType() {
        return type;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public T getDefaultValue() {
        return defaultValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        AchievementEntryImpl<?> that = (AchievementEntryImpl<?>) o;

        return new EqualsBuilder()
                .append(getId(), that.getId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .toHashCode();
    }
}
