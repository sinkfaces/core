package ru.tesker.game.impl;

import com.google.common.collect.ImmutableSet;
import ru.tesker.game.api.purchase.CustomerCart;
import ru.tesker.game.api.purchase.Purchase;

import java.util.HashSet;
import java.util.Set;

public class CustomerCartImpl implements CustomerCart {

    private Set<Purchase> purchases = new HashSet<>();


    @Override
    public Set<Purchase> getPurchases() {
        return ImmutableSet.copyOf(purchases);
    }

    @Override
    public void buy(Purchase purchase, String reason) {
        if(purchases.contains(purchase)) {
            throw new IllegalArgumentException(String.format("Purchase %s is already bought", purchase.getId()));
        }
        purchases.add(purchase);
    }

    @Override
    public boolean isBought(Purchase purchase, String reason) {
        return purchases.contains(purchase);
    }

}
