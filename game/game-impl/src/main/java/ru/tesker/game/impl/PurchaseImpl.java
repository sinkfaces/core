package ru.tesker.game.impl;

import com.google.common.util.concurrent.ListenableFuture;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import ru.tesker.game.api.Game;
import ru.tesker.game.api.GameManager;
import ru.tesker.game.api.purchase.Purchase;

import java.math.BigDecimal;
import java.util.Optional;

public class PurchaseImpl implements Purchase {

    private String id;
    private BigDecimal price;

    public PurchaseImpl(String id, BigDecimal price) {
        this.id = id;
        this.price = price;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PurchaseImpl purchase = (PurchaseImpl) o;

        return new EqualsBuilder()
                .append(getId(), purchase.getId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .toHashCode();
    }

    @Override
    public String toString() {
        return "PurchaseImpl{" +
                "id='" + id + '\'' +
                ", price=" + price +
                '}';
    }
}
