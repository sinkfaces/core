package ru.tesker.game.impl.achievement;

import com.datastax.driver.mapping.annotations.Param;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Table(keyspace = "mixmine", name = "player_achievements")
public class DataPlayerAchievement {

    @PartitionKey
    private String gameId;
    @PartitionKey(1)
    private UUID userId;
    @PartitionKey(2)
    private String contextId;
    private Map<String, String> values;

    public DataPlayerAchievement(String gameId, UUID userId, String contextId, Map<String, String> values) {
        this.gameId = gameId;
        this.userId = userId;
        this.contextId = contextId;
        this.values = values;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public Map<String, String> getValues() {
        return values;
    }

    public void setValues(Map<String, String> values) {
        this.values = values;
    }
}
