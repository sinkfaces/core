package ru.tesker.game.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tesker.game.api.Game;
import ru.tesker.game.api.GameManager;
import ru.tesker.game.impl.dao.GameDao;

import java.util.HashMap;
import java.util.Optional;
import java.util.Map;

@Component
public class GameManagerImpl implements GameManager<GameImpl> {

    private Map<String, GameImpl> games = new HashMap<>();
    private GameDao<GameImpl> gameDao;

    @Autowired
    public GameManagerImpl(GameDao<GameImpl> gameDao) {
        this.gameDao = gameDao;
    }


    @Override
    public ListenableFuture<Optional<GameImpl>> getGame(String id) {
        Preconditions.checkNotNull(id, "null id");
        GameImpl game =  games.get(id);
        if(game == null) {
            return gameDao.getGame(id);
        }
        return Futures.immediateFuture(Optional.of(game));
    }

    @Override
    public ListenableFuture<GameImpl> createGame(String id) throws IllegalArgumentException {
        Preconditions.checkNotNull(id, "null id");
        GameImpl game = new GameImpl(id);
        ListenableFuture<Void> save = gameDao.save(game);
        return Futures.transform(save, v -> game, MoreExecutors.directExecutor());
    }

    @Override
    public Map<String, GameImpl> getCachedGames() {
        return ImmutableMap.copyOf(games);
    }
}
