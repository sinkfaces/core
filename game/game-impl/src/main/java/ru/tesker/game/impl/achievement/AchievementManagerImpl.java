package ru.tesker.game.impl.achievement;

import com.google.common.util.concurrent.ListenableFuture;

import ru.tesker.game.api.achievement.AchievementContext;
import ru.tesker.game.api.achievement.AchievementManager;
import ru.tesker.game.api.achievement.Achievements;

import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;
import java.util.Map;


public class AchievementManagerImpl implements AchievementManager {

    private Map<String, AchievementContext> contextMap = new HashMap<>();

    @Override
    public Optional<AchievementContext> getContext(String id) {
        return Optional.ofNullable(contextMap.get(id));
    }

    @Override
    public AchievementContext createContext(String id) throws IllegalArgumentException {
        AchievementContext context = new AchievementContextImpl(id);
        contextMap.put(id, context);
        return context;
    }

    @Override
    public void save(AchievementContext achievementContext) {

    }



}
