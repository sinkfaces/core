package ru.tesker.game.impl.achievement;

import com.google.common.util.concurrent.ListenableFuture;
import org.bukkit.Achievement;
import ru.tesker.game.api.Game;
import ru.tesker.game.api.achievement.AchievementContext;
import ru.tesker.game.api.achievement.AchievementEntry;
import ru.tesker.game.api.achievement.Achievements;

import java.util.UUID;

public interface PlayerAchievementsDao<T extends Achievements> {

    ListenableFuture<T> getAchievements(Game game, UUID uuid, AchievementContext context);

    void save(T t);

}
