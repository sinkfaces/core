package ru.tesker.game.api;

import com.google.common.util.concurrent.ListenableFuture;

import java.util.Map;
import com.google.common.util.concurrent.MoreExecutors;
import ru.tesker.core.api.concurrent.FutureHelper;

import java.util.Optional;
import java.util.function.Supplier;

public interface GameManager<T extends Game> {

    ListenableFuture<Optional<T>> getGame(String id);

    ListenableFuture<T> createGame(String id) throws IllegalArgumentException;

    Map<String, T> getCachedGames();

    default ListenableFuture<T> getOrCreateGame(String id) {
        return FutureHelper.getOrCreateAsync(() -> getGame(id), () -> createGame(id));
    }

}
