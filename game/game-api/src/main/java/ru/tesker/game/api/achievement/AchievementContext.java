package ru.tesker.game.api.achievement;

import com.google.common.util.concurrent.ListenableFuture;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface AchievementContext {

    String getId();

    Collection<AchievementEntry<?>> getEntries();

    <T> Optional<AchievementEntry<T>> getEntry(String id, Class<T> type);

    <T> AchievementEntry<T> createEntry(String id, Class<T> type, T def);

    default <T> AchievementEntry<T> getOrCreateEntry(String id, Class<T> type, T def) {
        return getEntry(id, type).orElseGet(() -> createEntry(id, type, def));
    }


}
