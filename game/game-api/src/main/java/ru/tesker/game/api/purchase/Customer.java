package ru.tesker.game.api.purchase;

import ru.tesker.game.api.Purse;

public interface Customer {

    Purse getPurse();

    CustomerCart getPurchases();


}
