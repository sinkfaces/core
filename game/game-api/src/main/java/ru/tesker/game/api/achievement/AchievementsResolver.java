package ru.tesker.game.api.achievement;

import com.google.common.util.concurrent.ListenableFuture;

import java.util.UUID;

public interface AchievementsResolver {

    ListenableFuture<Achievements> getAchievements(AchievementContext context, UUID uuid);

    void save(Achievements achievements);

}
