package ru.tesker.game.api.achievement;

public interface Achievements {

    <T> T getValue(AchievementEntry<T> entry);

    <T> boolean hasValue(AchievementEntry<T> entry);

    <T> void setValue(AchievementEntry<T> achievementEntry, T value);

    AchievementContext getContext();

}
