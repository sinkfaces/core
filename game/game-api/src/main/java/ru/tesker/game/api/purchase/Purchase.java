package ru.tesker.game.api.purchase;

import java.math.BigDecimal;

public interface Purchase {

    String getId();

    BigDecimal getPrice();

}
