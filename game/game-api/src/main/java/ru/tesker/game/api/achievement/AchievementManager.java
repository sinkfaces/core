package ru.tesker.game.api.achievement;

import com.google.common.util.concurrent.ListenableFuture;

import java.util.Optional;
import java.util.UUID;

public interface AchievementManager {

    Optional<AchievementContext> getContext(String id);

    AchievementContext createContext(String id) throws IllegalArgumentException;

    default AchievementContext getOrCreateContext(String id) {
        return getContext(id).orElseGet(() -> createContext(id));
    }

    void save(AchievementContext achievementContext);



}
