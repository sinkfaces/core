package ru.tesker.game.api;

import com.google.common.util.concurrent.ListenableFuture;
import ru.tesker.game.api.purchase.Customer;

import java.util.UUID;

public interface TradeManager {

    ListenableFuture<Customer> getCustomer(UUID uuid);


}
