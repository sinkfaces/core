package ru.tesker.game.api;

import com.google.common.base.Preconditions;
import ru.tesker.game.api.purchase.PurchaseManager;

public class GameApi {

    private static GameApi instance;
    private GameManager<Game> gameManager;
    private TradeManager tradeManager;
    private PurchaseManager purchaseManager;

    public GameApi(GameManager<Game> gameManager,
                   TradeManager tradeManager,
                   PurchaseManager purchaseManager) {
        Preconditions.checkState(instance == null, "GameApi is already initialized");
        instance = this;
        this.purchaseManager = purchaseManager;
        this.gameManager = gameManager;
        this.tradeManager = tradeManager;
    }

    public static PurchaseManager getPurchaseManager() {
        return getInstance().purchaseManager;
    }

    public static TradeManager getTradeManager() {
        return getInstance().tradeManager;
    }

    public static GameManager<Game> getGameManager() {
        return getInstance().gameManager;
    }

    private static GameApi getInstance() {
        Preconditions.checkNotNull(instance, "GameApi isn't initialized yet");
        return instance;
    }

}
