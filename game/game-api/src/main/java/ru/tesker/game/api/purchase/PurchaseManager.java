package ru.tesker.game.api.purchase;

public interface PurchaseManager {

    Purchase getPurchase(String id);

    Purchase createPurchase(PurchaseBuilder purchaseBuilder);




}
