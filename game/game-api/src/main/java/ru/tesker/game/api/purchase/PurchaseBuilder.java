package ru.tesker.game.api.purchase;

import ru.tesker.game.api.GameApi;

import java.math.BigDecimal;
import java.util.function.Function;

public class PurchaseBuilder {

    private final Function<PurchaseBuilder, Purchase> creator;
    private String id;
    private BigDecimal price;

    private PurchaseBuilder() {
        creator = (builder) -> GameApi.getPurchaseManager().createPurchase(builder);
    }

    private PurchaseBuilder(Function<PurchaseBuilder, Purchase> creator) {
        this.creator = creator;
    }

    public PurchaseBuilder setId(String id) {
        this.id = id;
        return this;
    }
    public PurchaseBuilder setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getId() {
        return id;
    }

    public Purchase build() {
        return creator.apply(this);
    }

    public static PurchaseBuilder create() {
        return new PurchaseBuilder();
    }

    public static PurchaseBuilder create(Function<PurchaseBuilder, Purchase> creator) {
        return new PurchaseBuilder(creator);
    }
}
