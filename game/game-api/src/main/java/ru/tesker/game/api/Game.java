package ru.tesker.game.api;

import ru.tesker.game.api.achievement.AchievementManager;
import ru.tesker.game.api.achievement.AchievementsResolver;

public interface Game {

    String getId();

    AchievementManager getAchievementManager();

    AchievementsResolver getAchievementsResolver();

}
