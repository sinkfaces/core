package ru.tesker.game.api.achievement;


import org.springframework.core.convert.converter.Converter;

public interface AchievementEntry<T> {

    Class<T> getType();

    String getId();

    T getDefaultValue();


}
