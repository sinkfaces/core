package ru.tesker.game.api.purchase;

import java.util.Set;

public interface CustomerCart {

    Set<Purchase> getPurchases();

    void buy(Purchase purchase, String reason);

    boolean isBought(Purchase purchase, String reason);


}
